#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "efface_ecran.h"

int main(int argc, char *argv[])
{

	efface_ecran();
	int tab[3][9];

	//fc time_init() : initialisation de la fonction rand 	
	time_t t1;
	(void)time(&t1);
	srand((long)t1);
 
 //fc create_random_nbrs() création des nombres aléatoires dans chaque case TOUS DIFFERENTS

	int i = 0;
	int bc3 = 0;
	int bc2 = 0;
	int bc1 = 0;
	while(i <= 2)
	{
		switch(i)
		{
			case 0 :
				
				while(bc2 <= 8)
				{

				tab[i][bc2] = bc2*10 + rand()%9+1; 
				bc2 = bc2 + 1;

				}
			break ;

			case 1 :
				
				while(bc1 <= 8)
				{

					tab[i][bc1] = bc1*10 + rand()%9+1; 

						while(tab[i][bc1] == tab[i-1][bc1])
						{
							tab[i][bc1] = bc1*10 + rand()%9+1; 
						}

					bc1 = bc1 + 1;

				}
				
	    
			break;

			case 2 :
			
				while(bc3 <= 8)
				{

					tab[i][bc3] = bc3*10 + rand()%9+1; 
					
						while(tab[i][bc3] == tab[i-1][bc3] || tab[i][bc3] == tab[i-2][bc3])
						{
							tab[i][bc3] = bc3*10 + rand()%9+1; 
						}

					bc3 = bc3 + 1;

				}
			break;
		}
	i = i + 1;
	}
	
//fc create_blank() : Met 4 case vide par ligne (aléatoire)

	int k = 0; 
	while(k <= 2)
	{
		int n1; 
		int n2;
		int n3; 
		int n4;
		n1 = rand()%8+1;
		n2 = rand()%8+1;
			while(n2 == n1)
			{
				n2 = rand()%8+1;
			}

		n3 = rand()%8+1;
			while(n3 == n1 || n3 == n2)
			{
				n3 = rand()%8+1;
			}
		n4 = rand()%8+1;
			while(n4 == n1 || n4 == n2 || n4 == n3)
			{
				n4 = rand()%8+1;
			}
		
		tab[k][n1] = 0;
		tab[k][n2] = 0;
		tab[k][n3] = 0;
		tab[k][n4] = 0;	
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;

		k = k + 1 ;
	}
	//fc show_grid() affiche la grille

	int c = 0 ;
	int l = 0 ;
	
	while(l <= 2)
	{
		while(c <= 8)
			{
				if(tab[l][c] < 10)
					{
						fprintf(stdout,"| 0%d |",tab[l][c]);
					}
				else fprintf(stdout,"| %d |",tab[l][c]);

			c = c + 1;

			}

		c = 0; //reinitialise la variable c à 0
		l = l + 1;

		fprintf(stdout,"\n");

	}
	
	//fc tirage()  

	//initialisation de la fonction rand 	
	int val = 1 ;
	int tab_valeur[99];

	//cree un tableau avec des valeurs de 0 à 99
	while(val <= 99)
	{
		tab_valeur[val - 1] = val;
		val = val + 1;
	}
	int nbr_tirage = 0;
	int count = 0;
	int cp = 0;
	while(cp < 99) // sera remplacer par la condition TANT QUE tous nbrs de la grille pas tirée alors faire
	{
		fprintf(stdout,"1 pour continuer \n2 pour instantané(BETA_0.0.1) \n3 pour quitter \n");
		int key;
		scanf("%d", &key);
		switch(key)
		{
			case 1 : //continuer

					//tire nombre aléatoire
					nbr_tirage = rand()%99+1;

					//test qu'il n'a pas déjà éte tirée
						while(tab_valeur[nbr_tirage - 1] <= 0)
						{
							nbr_tirage = rand()%99+1;
						}
					//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
					tab_valeur[nbr_tirage - 1] = 0;
					
					//affichage nbr gagant :
					fprintf(stdout,"Le numéro gagant est le %d !\n \n", nbr_tirage);
					
					printf("\b");

			break;

			case 2: //mode intantané
					while(count <= 99)
					{ 
						nbr_tirage = rand()%99+1;

						//test qu'il n'a pas déjà éte tirée
							while(tab_valeur[nbr_tirage - 1] <= 0)
							{
								nbr_tirage = rand()%99+1;
							}
						//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
						tab_valeur[nbr_tirage - 1] = 0;
						
						//affichage nbr gagant :
						fprintf(stdout,"Le numéro gagant est le %d !\n \n", nbr_tirage);
						
						printf("\b");
						count = count + 1;
					}
			break;

			case 3: // quitter
					cp = 99;
			break;
			break;
		} // switch END
	cp = cp + 1;
	} //while END
	return 0;
}// main END
	