
#ifndef __DEFINITION_H
#define __DEFINITION_H

#define MY_IMAGE(x) img ## x

#define IMG_SIZE 110
#define SECOND_IMG_SIZE 60


void definition(void);

#define IMAGE_PATH0  "../images/0.png"
#define IMAGE_PATH1  "../images/1.png"
#define IMAGE_PATH2  "../images/2.png"
#define IMAGE_PATH3  "../images/3.png"
#define IMAGE_PATH4  "../images/4.png"
#define IMAGE_PATH5  "../images/5.png"
#define IMAGE_PATH6  "../images/6.png"
#define IMAGE_PATH7  "../images/7.png"
#define IMAGE_PATH8  "../images/8.png"
#define IMAGE_PATH9  "../images/9.png"
#define IMAGE_PATH10 "../images/10.png"
#define IMAGE_PATH11 "../images/11.png"
#define IMAGE_PATH12 "../images/12.png"
#define IMAGE_PATH13 "../images/13.png"
#define IMAGE_PATH14 "../images/14.png"
#define IMAGE_PATH15 "../images/15.png"
#define IMAGE_PATH16 "../images/16.png"
#define IMAGE_PATH17 "../images/17.png"
#define IMAGE_PATH18 "../images/18.png"
#define IMAGE_PATH19 "../images/19.png"
#define IMAGE_PATH20 "../images/20.png"
#define IMAGE_PATH21 "../images/21.png"
#define IMAGE_PATH22 "../images/22.png"
#define IMAGE_PATH23 "../images/23.png"
#define IMAGE_PATH24 "../images/24.png"
#define IMAGE_PATH25 "../images/25.png"
#define IMAGE_PATH26 "../images/26.png"
#define IMAGE_PATH27 "../images/27.png"
#define IMAGE_PATH28 "../images/28.png"
#define IMAGE_PATH29 "../images/29.png"
#define IMAGE_PATH30 "../images/30.png"
#define IMAGE_PATH31 "../images/31.png"
#define IMAGE_PATH32 "../images/32.png"
#define IMAGE_PATH33 "../images/33.png"
#define IMAGE_PATH34 "../images/34.png"
#define IMAGE_PATH35 "../images/35.png"
#define IMAGE_PATH36 "../images/36.png"
#define IMAGE_PATH37 "../images/37.png"
#define IMAGE_PATH38 "../images/38.png"
#define IMAGE_PATH39 "../images/39.png"
#define IMAGE_PATH40 "../images/40.png"
#define IMAGE_PATH41 "../images/41.png"
#define IMAGE_PATH42 "../images/42.png"
#define IMAGE_PATH43 "../images/43.png"
#define IMAGE_PATH44 "../images/44.png"
#define IMAGE_PATH45 "../images/45.png"
#define IMAGE_PATH46 "../images/46.png"
#define IMAGE_PATH47 "../images/47.png"
#define IMAGE_PATH48 "../images/48.png"
#define IMAGE_PATH49 "../images/49.png"
#define IMAGE_PATH50 "../images/50.png"
#define IMAGE_PATH51 "../images/51.png"
#define IMAGE_PATH52 "../images/52.png"
#define IMAGE_PATH53 "../images/53.png"
#define IMAGE_PATH54 "../images/54.png"
#define IMAGE_PATH55 "../images/55.png"
#define IMAGE_PATH56 "../images/56.png"
#define IMAGE_PATH57 "../images/57.png"
#define IMAGE_PATH58 "../images/58.png"
#define IMAGE_PATH59 "../images/59.png"
#define IMAGE_PATH60 "../images/60.png"
#define IMAGE_PATH61 "../images/61.png"
#define IMAGE_PATH62 "../images/62.png"
#define IMAGE_PATH63 "../images/63.png"
#define IMAGE_PATH64 "../images/64.png"
#define IMAGE_PATH65 "../images/65.png"
#define IMAGE_PATH66 "../images/66.png"
#define IMAGE_PATH67 "../images/67.png"
#define IMAGE_PATH68 "../images/68.png"
#define IMAGE_PATH69 "../images/69.png"
#define IMAGE_PATH70 "../images/70.png"
#define IMAGE_PATH71 "../images/71.png"
#define IMAGE_PATH72 "../images/72.png"
#define IMAGE_PATH73 "../images/73.png"
#define IMAGE_PATH74 "../images/74.png"
#define IMAGE_PATH75 "../images/75.png"
#define IMAGE_PATH76 "../images/76.png"
#define IMAGE_PATH77 "../images/77.png"
#define IMAGE_PATH78 "../images/78.png"
#define IMAGE_PATH79 "../images/79.png"
#define IMAGE_PATH80 "../images/80.png"
#define IMAGE_PATH81 "../images/81.png"
#define IMAGE_PATH82 "../images/82.png"
#define IMAGE_PATH83 "../images/83.png"
#define IMAGE_PATH84 "../images/84.png"
#define IMAGE_PATH85 "../images/85.png"
#define IMAGE_PATH86 "../images/86.png"
#define IMAGE_PATH87 "../images/87.png"
#define IMAGE_PATH88 "../images/88.png"
#define IMAGE_PATH89 "../images/89.png"
#define IMAGE_PATH90 "../images/90.png"
#define IMAGE_PATH91 "../images/91.png"
#define IMAGE_PATH92 "../images/92.png"
#define IMAGE_PATH93 "../images/93.png"
#define IMAGE_PATH94 "../images/94.png"
#define IMAGE_PATH95 "../images/95.png"
#define IMAGE_PATH96 "../images/96.png"
#define IMAGE_PATH97 "../images/97.png"
#define IMAGE_PATH98 "../images/98.png"
#define IMAGE_PATH99 "../images/99.png"
#define IMAGE_PATH100 "../images/100.png"

const char * IMAGE_PATH[101] =
{
    IMAGE_PATH0,  IMAGE_PATH1,  IMAGE_PATH2,  IMAGE_PATH3,  IMAGE_PATH4,
    IMAGE_PATH5,  IMAGE_PATH6,  IMAGE_PATH7,  IMAGE_PATH8,  IMAGE_PATH9,
    IMAGE_PATH10, IMAGE_PATH11, IMAGE_PATH12, IMAGE_PATH13, IMAGE_PATH14,
    IMAGE_PATH15, IMAGE_PATH16, IMAGE_PATH17, IMAGE_PATH18, IMAGE_PATH19,
    IMAGE_PATH20, IMAGE_PATH21, IMAGE_PATH22, IMAGE_PATH23, IMAGE_PATH24,
    IMAGE_PATH25, IMAGE_PATH26, IMAGE_PATH27, IMAGE_PATH28, IMAGE_PATH29,
    IMAGE_PATH30, IMAGE_PATH31, IMAGE_PATH32, IMAGE_PATH33, IMAGE_PATH34,
    IMAGE_PATH35, IMAGE_PATH36, IMAGE_PATH37, IMAGE_PATH38, IMAGE_PATH39,
    IMAGE_PATH40, IMAGE_PATH41, IMAGE_PATH42, IMAGE_PATH43, IMAGE_PATH44,
    IMAGE_PATH45, IMAGE_PATH46, IMAGE_PATH47, IMAGE_PATH48, IMAGE_PATH49,
    IMAGE_PATH50, IMAGE_PATH51, IMAGE_PATH52, IMAGE_PATH53, IMAGE_PATH54,
    IMAGE_PATH55, IMAGE_PATH56, IMAGE_PATH57, IMAGE_PATH58, IMAGE_PATH59,
    IMAGE_PATH60, IMAGE_PATH61, IMAGE_PATH62, IMAGE_PATH63, IMAGE_PATH64,
    IMAGE_PATH65, IMAGE_PATH66, IMAGE_PATH67, IMAGE_PATH68, IMAGE_PATH69,
    IMAGE_PATH70, IMAGE_PATH71, IMAGE_PATH72, IMAGE_PATH73, IMAGE_PATH74,
    IMAGE_PATH75, IMAGE_PATH76, IMAGE_PATH77, IMAGE_PATH78, IMAGE_PATH79,
    IMAGE_PATH80, IMAGE_PATH81, IMAGE_PATH82, IMAGE_PATH83, IMAGE_PATH84,
    IMAGE_PATH85, IMAGE_PATH86, IMAGE_PATH87, IMAGE_PATH88, IMAGE_PATH89,
    IMAGE_PATH90, IMAGE_PATH91, IMAGE_PATH92, IMAGE_PATH93, IMAGE_PATH94,
    IMAGE_PATH95, IMAGE_PATH96, IMAGE_PATH97, IMAGE_PATH98, IMAGE_PATH99,
    IMAGE_PATH100

};


#endif /* __DEFINITION_H */
