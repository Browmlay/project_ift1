/*
 * Fichier d'en tête bingo.h pour le projet ift1
 * Auteur : Eric Bachard  / vendredi 27 décembre 2019, 23:09:00 (UTC+0100)
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __BINGO__H__
#define __BINGO__H__

void tirage_bingo(int tab[][9]);

#endif /* __BINGO__H__ */

