
//problème ligne 782

// dear imgui: standalone example application for SDL2 + OpenGL
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (SDL is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no standard header to access modern OpenGL functions easily. Alternatives are GLEW, Glad, etc.)


#include <SDL.h>
#include <SDL/SDL_mixer.h>

#include "definition.h"
#include "imgui.h"
// #include "musique.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>

#include <GL/gl3w.h>
#include <GL/gl.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <math.h>
#include <time.h>
#include "saisie.h" //copyright Eric BACHARD

typedef enum NumeroLigne
{
    PREMIERE_LIGNE  = 0,
    DEUXIEME_LIGNE  = 1,
    TROISIEME_LIGNE = 2
} NUMERO_LIGNE;

/****Impossible de faire GLuint[100] et textureID[]****/

GLuint textureID[101] = { 0 };





/**********************/


/*
FIXME : initialize the array
static void initializeTextureID(void)
{
    for (int i = 0 ; i < 100 ; i++)
    {
        textureID[i] = 0;
    }

}
*/

int my_image_height = 100;
int my_image_width = 100;

// Simple helper function to load an image into a OpenGL texture with common settings
// https://github.com/ocornut/imgui/wiki/Image-Loading-and-Displaying-Examples

static bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height)
{
    // Load from file
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width = image_width;
    *out_height = image_height;

    return true;
}


//link to secondarys functions used in the main.cpp



// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

int tab[3][9];




static int ligne = 0;
static int colonne = 0;

static void tirage_bingo(void);

static void tirage_bingo(void)
{
/*************************ALGO DU BINGO FONCTIONNEL***************************/
    
   
    NUMERO_LIGNE ligne = PREMIERE_LIGNE;


    for (ligne = 0 ; ligne < 3; ligne++)
    {

        switch(ligne)
        {
            case PREMIERE_LIGNE :
                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[PREMIERE_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                }
            break ;

            case DEUXIEME_LIGNE :
                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

                    while(tab[DEUXIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
                    {
                        tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                    }
                }
            break;

            case TROISIEME_LIGNE:

                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

                    while(tab[TROISIEME_LIGNE][colonne] == tab[DEUXIEME_LIGNE][colonne] || tab[TROISIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
                    {
                        tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                    }
                }
            break;

            default:
            break;
        }
    }

    
    
    //fc create_blank() : Met 4 cases vides par ligne (aléatoire)
    ligne = 0;
    for (ligne = 0 ; ligne < 3; ligne ++)
    
    {
        int case_vide1 = 0;
        int case_vide2 = 0;
        int case_vide3 = 0;
        int case_vide4 = 0;
        case_vide1 = rand()%8+1;
        case_vide2 = rand()%8+1;
            while(case_vide2 == case_vide1)
            {
                case_vide2 = rand()%8+1;
            }

        case_vide3 = rand()%8+1;
            while(case_vide3 == case_vide1 || case_vide3 == case_vide2)
            {
                case_vide3 = rand()%8+1;
            }
        case_vide4 = rand()%8+1;
            while(case_vide4 == case_vide1 || case_vide4 == case_vide2 || case_vide4 == case_vide3)
            {
                case_vide4 = rand()%8+1;
            }
        
        tab[ligne][case_vide1] = 0;
        tab[ligne][case_vide2] = 0;
        tab[ligne][case_vide3] = 0;
        tab[ligne][case_vide4] = 0;
        case_vide1 = 0;
        case_vide2 = 0;
        case_vide3 = 0;
        case_vide4 = 0;

    }


    //fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
    
}
/*SEGMENTATION FAULT
Mix_Music* myMusic;
char MP3_FULL_NAME[23];
int music_init(void);


int music_init(void)
{
  Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024);
  myMusic = Mix_LoadMUS(MP3_FULL_NAME);
  Mix_PlayMusic(myMusic, -1);
  return EXIT_SUCCESS;
}

*/


// Main code
int main(void)
{


/* Segmentation fault
music_init();

Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024);
  myMusic = Mix_LoadMUS("../sound/undertale.mp3");
  Mix_PlayMusic(myMusic, -1);
*/




/***********************************/
    time_t t1;
    (void)time(&t1);
    srand((long)t1);

    



/***********************************/

    // Setup SDL
    // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
    // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }

    // Decide GL+GLSL versions
#if __APPLE__
    // GL 3.2 Core + GLSL 150
    const char* glsl_version = "#version 150";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Window* window = SDL_CreateWindow("Dear ImGui SDL2+OpenGL3 example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'misc/fonts/README.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //    io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    io.Fonts->AddFontFromFileTTF("../fonts/DroidSans.ttf", 18.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    for (int i = 0 ; i < 101 ; i++)
    {
        bool MY_IMAGE(i) = LoadTextureFromFile(IMAGE_PATH[i], &textureID[i], &my_image_width, &my_image_height);
        IM_ASSERT(MY_IMAGE(i));
    }

    // Our state
    // bool show_demo_window = true;
    bool show_another_window = false;

    // Main loop
    bool done = false;
    while (!done)
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        
        // if (show_demo_window)
        //     ImGui::ShowDemoWindow(&show_demo_window);

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            /*****************************************************************/
            ImGui::Begin("Hello, world!");
            ImGui::Text("Page de connection pour le BINGO"); 

            static char str0[128] = "";
            static char pseudo[128] = "nico";
            static char bufpass[128] = "";
            static char mdp[128] = "nico";

            ImGui::InputText("username", str0, IM_ARRAYSIZE(str0));
            ImGui::InputText("password", bufpass, 64, ImGuiInputTextFlags_Password | ImGuiInputTextFlags_CharsNoBlank);

            int result = 0;
            int result1 = 0;
            int tab_affichage[99];
            static int nombre_boules_deja_sortie = 0;
            int nbr_tire = 0;
            

            // comparing strings str1 and str2
            result = strcmp(str0, pseudo);
            // comparing strings str1 and str3
            result = strcmp(bufpass, mdp);

            


            if( result == 0 && result1 == 0 )
            {
                static int clicked = 0;
                static bool nouveau_tirage = true;
                static bool nouveau_tirage_de_boules = false;
                

                if (ImGui::Button("Jouer !"))
                    clicked++;

                /**Renvoie un tableau tab[3][9] avec 3 ligne et 9 colonnes avec à l'intérieur des valeurs aléatoires */
                if (nouveau_tirage == true)
                {
                    tirage_bingo();
#ifdef DEBUG        
                    for (int l = 0 ; l < 3 ; l++)
                    {
                       for (int m = 0 ; m < 9 ; m++)
                       {
                           fprintf(stdout, "tab[%d][%d] = %d  ;", l, m, tab[l][m]);
                       }
                       fprintf(stdout, "\n");
                    }
#endif
                    nouveau_tirage = false;
                }


                
                    colonne = 0;
                    ligne = 0;
                    int vlr_liste = 0;
                    int tab_grid[15] = { 0 };

                    while(ligne < 3)
                    {
                        while(colonne < 9)
                        {
                            
                            if(tab[ligne][colonne] != 0)
                            {
                                tab_grid[vlr_liste] = tab[ligne][colonne];
                                vlr_liste = vlr_liste + 1;
                            }
                            colonne = colonne + 1;
                        }
                        colonne = 0;
                        ligne = ligne + 1;
                    }
                    

                if(nouveau_tirage_de_boules == true)
                {

                /****Comptage de nombre trouver****/

                    //fonction créer liste valeurs à trouver
                    int liste_valeurs_pouvent_etre_trouver[100];
                    for(int val = 1 ; val < 100 ; val ++)
                    {
                        liste_valeurs_pouvent_etre_trouver[val - 1] = val;
                    }

                    
                    nbr_tire = rand()%99+1;

                    //test qu'il n'a pas déjà éte tiré
                    while(liste_valeurs_pouvent_etre_trouver[nbr_tire - 1] == 0)
                    {
                        nbr_tire = rand()%99+1;
                    }
                    //le nombre tiré dans le tableau va être remplacé par un zéro pour ne plus être choisi
                    liste_valeurs_pouvent_etre_trouver[nbr_tire - 1] = 0;


                    tab_affichage[nombre_boules_deja_sortie] = nbr_tire ;
                    nombre_boules_deja_sortie = nombre_boules_deja_sortie + 1;


                    int id_valeur = 0;
                    int nbr_boules_restantes_a_trouver = 15;
                    while(id_valeur < 15) // comptage boules trouvées (comparaison des deux grilles)
                    {   
                        if( nbr_tire == tab_grid[id_valeur] )
                        {
                            nbr_boules_restantes_a_trouver = nbr_boules_restantes_a_trouver - 1;
                        }//if END
                        id_valeur = id_valeur + 1;
                    }
                    id_valeur = 0;

                    

                    //DEBUG
                    /*
                    for (int a = 1 ; a < 100 ; a ++)
                    {
                        fprintf(stdout, "liste_valeurs_pouvent_etre_trouver[%d - 1] = %d;\n", nbr_tire,liste_valeurs_pouvent_etre_trouver[a - 1] );
                        if(a == 99)
                        fprintf(stdout, "\n");
                    }
                    */
                    // fprintf(stdout, "nombre_boules_deja_sortie = %d,tab_affichage[nombre_boules_deja_sortie] = %d \n",nombre_boules_deja_sortie, nbr_tire);
                    

                   
                    /****Fin comptage de nombre trouver****/

                    nouveau_tirage_de_boules = false;
                }


                if (clicked > 0)
                {

                    ImGui::Begin("BINGO le Jeu", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
                    ImGui::Text("Bienvenu sur BINGO le jeu");

                    int numero_de_ligne = 0;
                    int numero_de_colonne = 0;
          

                    for (numero_de_ligne = 0 ; numero_de_ligne< 3 ; numero_de_ligne++)
                    {
                        for (numero_de_colonne = 0 ; numero_de_colonne < 9 ; numero_de_colonne++)
                        {
                            ImGui::Image((void*)(intptr_t)textureID[tab[numero_de_ligne][numero_de_colonne]], ImVec2(IMG_SIZE, IMG_SIZE));

                            if(numero_de_colonne < 8)
                                ImGui::SameLine();
                        }
                    }


                     


                    /****Partie Graphique****/
                    ImGui::Image((void*)(intptr_t)textureID[100], ImVec2(1050, 8));

                    ImGui::NewLine();
                    ImGui::Text("Il y a %d numéro de sortie",nombre_boules_deja_sortie); 

                    ImGui::NewLine();
                    ImGui::Text("Il reste %d numéro à touver",nbr_boules_restantes_a_trouver); 

                    ImGui::NewLine();
                    ImGui::Image((void*)(intptr_t)textureID[100], ImVec2(1050, 8));
                    /****Fin partie graphique****/


                     //affichage boules sortie
                    for (int q = 0; q < nombre_boules_deja_sortie ; q++)
                    {
                        ImGui::Image((void*)(intptr_t)textureID[tab_affichage[q]], ImVec2(SECOND_IMG_SIZE, SECOND_IMG_SIZE));
                        if(q != 14 && q != 29 && q != 44 && q != 59 && q != 74 && q != 89 )
                        {
                            ImGui::SameLine();
                        }
                    }


                    ImGui::NewLine();
                    if(nombre_boules_deja_sortie < 99)
                    {
                        if (ImGui::Button("Tirée un nombre"))
                            nouveau_tirage_de_boules = true;
                    }
                    
                    if(nombre_boules_deja_sortie < 1)
                    {                   
                        ImGui::NewLine();
                        if (ImGui::Button("Nouveau tirage ?"))
                            nouveau_tirage = true;
                    }

                    if (ImGui::Button("Sortir ?"))
                    {
                        for (int i = 0 ; i <128 ; i++)
                        {
                            str0[i]    = '\0';
                            bufpass[i] = '\0';
                        nombre_boules_deja_sortie = 0;
                        }
                        clicked = 0;
                    }
                    ImGui::End();
                }
            }
            ImGui::End();

            // Rendering
            ImGui::Render();
            glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
            glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
            glClear(GL_COLOR_BUFFER_BIT);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
            SDL_GL_SwapWindow(window);

            /*****************************************************************/

            // limit to ~ 60 fps is enough
            SDL_Delay(15);
        }
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);

    ////////fin audio
    Mix_CloseAudio(); //Fermeture de l'API


    SDL_Quit();

    return 0;
}

