/*
 * files : Bingo_X.X.X.c
 * Author : Nicolas SAINTVOIRIN, ADRIEN HUYGHEBAERT 
 * This project is licensed under the GNU General Public License v3.0.
 * See : https://choosealicense.com/licenses/gpl-3.0/
 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "saisie.h" //copyright Eric BACHARD
#include "efface_ecran.h"


typedef enum NumeroLigne
{
    PREMIERE_LIGNE  = 0,
    DEUXIEME_LIGNE  = 1,
    TROISIEME_LIGNE = 2
} NUMERO_LIGNE;


// declaration forward
void color_red(void);
void color_reset(void);


//internal functions
void color_red ()
{
    fprintf(stdout, "\033[1;31m");
}



void color_reset ()
{
    fprintf(stdout, "\033[0m");
}





//main function
int main(void)
{
    int colonne = 0;
    efface_ecran();
    int tab[3][9];

    //fc time_init() : initialisation de la fonction rand 	
    time_t t1;
    (void)time(&t1);
    srand((long)t1);

    //fc create_random_nbrs() création des nombres aléatoires dans chaque case TOUS DIFFERENTS

    NUMERO_LIGNE ligne = PREMIERE_LIGNE;


    for (ligne = 0 ; ligne < 3; ligne++)
    {

        switch(ligne)
        {
            case PREMIERE_LIGNE :
                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[PREMIERE_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                }
            break ;

            case DEUXIEME_LIGNE :
                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

                    while(tab[DEUXIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
                    {
                        tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                    }
                }
            break;

            case TROISIEME_LIGNE:

                for (colonne = 0 ; colonne < 9 ; colonne++)
                {
                    tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

                    while(tab[TROISIEME_LIGNE][colonne] == tab[DEUXIEME_LIGNE][colonne] || tab[TROISIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
                    {
                        tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
                    }
                }
            break;

            default:
            break;
        }
    }

	
	//fc create_blank() : Met 4 cases vide par ligne (aléatoire)
	ligne = 0;
	for (ligne = 0 ; ligne < 3; ligne ++)
	
	{
		int case_vide1 = 0;
		int case_vide2 = 0;
		int case_vide3 = 0;
		int case_vide4 = 0;
		case_vide1 = rand()%8+1;
		case_vide2 = rand()%8+1;
			while(case_vide2 == case_vide1)
			{
				case_vide2 = rand()%8+1;
			}

		case_vide3 = rand()%8+1;
			while(case_vide3 == case_vide1 || case_vide3 == case_vide2)
			{
				case_vide3 = rand()%8+1;
			}
		case_vide4 = rand()%8+1;
			while(case_vide4 == case_vide1 || case_vide4 == case_vide2 || case_vide4 == case_vide3)
			{
				case_vide4 = rand()%8+1;
			}
		
		tab[ligne][case_vide1] = 0;
		tab[ligne][case_vide2] = 0;
		tab[ligne][case_vide3] = 0;
		tab[ligne][case_vide4] = 0;
		case_vide1 = 0;
		case_vide2 = 0;
		case_vide3 = 0;
		case_vide4 = 0;

	}
	//fc show_grid() affiche la grille

	 colonne = 0 ;
	 ligne = 0 ;
	
	while(ligne < 3)
	{
		while(colonne < 9)
			{
				if(tab[ligne][colonne] < 10)
					{
						fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
					}
				else fprintf(stdout,"| %d |",tab[ligne][colonne]);

			colonne = colonne + 1;

			}

		colonne = 0; //reinitialise la variable c à 0
		ligne = ligne + 1;

		fprintf(stdout,"\n");

	}
	
	//fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
	colonne = 0;
	ligne = 0;
	int vlr_liste = 0;
	int tab_grid[15]; //15 nbr dans la grille (case vide enlever)
	while(ligne < 3)
		{
			while(colonne < 9)
			{
				if(tab[ligne][colonne] != 0)
				{
					tab_grid[vlr_liste] = tab[ligne][colonne];
					vlr_liste = vlr_liste + 1;
				}
				colonne = colonne + 1;
			}
			colonne = 0;
			ligne = ligne + 1;
		}

					
	

	//fonction créer tableau 1 dim
	int val = 1 ;
	int tab_valeur[100];

	//cree un tableau avec des valeurs de 0 à 99
	while(val < 100)
	{
		tab_valeur[val - 1] = val;
		val = val + 1;
	}

	//fc tirage
	int nbr_tirage = 0;
	int nbr_boules_restants = 15;
	int id_valeur = 0 ;
	int step = 0;
	int tab_affichage[100] ; // tableau d'affichage des nbrs tirées
	int p = 0 ;
	int q = 0 ;
	int r = 0 ;
	int val_print = 0 ;
	int congrate = 0;
	
	while(nbr_boules_restants > 0 || step > 99) //pour etre sur de sortir de la boucle à  step 98
	{
		
		fprintf(stdout,"\n");
		fprintf(stdout,"Les boules déjà tirées sont :");
		fprintf(stdout,"\n");		
		fprintf(stdout,"\nBoules tirées : %d     Boules restantes à trouver : %d \n",step,nbr_boules_restants );
		fprintf(stdout,"1 pour continuer \n2 pour instantané(BETA_0.0.1) \n3 pour quitter \n");
		
		int key = 0;
		key = saisie_nombre_entier_court(1,3);

		switch(key) //condition pour faire avancer le tirage
		{
			case 1 :
				//tire nombre aléatoire
				nbr_tirage = rand()%99+1;

				//test qu'il n'a pas déjà éte tiré
				while(tab_valeur[nbr_tirage - 1] == 0)
				{
					nbr_tirage = rand()%99+1;
				}
				tab_affichage[p] = nbr_tirage ; 
				p = p + 1;

				//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
				tab_valeur[nbr_tirage - 1] = 0;
							
				
				step = step + 1; //compte nbr boule sortie
				while(id_valeur < 15) // comptage boulles trouvée (comparaison des deux grilles)
				{	
					if( nbr_tirage == tab_grid[id_valeur] )
					{
						nbr_boules_restants = nbr_boules_restants - 1;
					}//if END
					id_valeur = id_valeur + 1;
				}		
				id_valeur = 0;


			//AFFICHAGE
			//affiche la grille
			efface_ecran();
			colonne = 0 ;
			ligne = 0 ;
			while(ligne < 3)
			{
				while(colonne < 9)
					{
						if(tab[ligne][colonne] < 10)
							{
								fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
							}
						else fprintf(stdout,"| %d |",tab[ligne][colonne]);
					colonne = colonne + 1;
					
					}
				fprintf(stdout,"\n");
				colonne = 0; //reinitialise la variable c à 0
				ligne = ligne + 1;
			}//fin affichage grille
			
			// AFFICHAGE boules sorties 
			while(q < p)
				{
					while(r < 15)
					{
						if(tab_affichage[q] == tab_grid[r])// afficher les nombres bons en rouge
						{
							color_red();
							fprintf(stdout," %d ;",tab_affichage[q]);
							color_reset();
							val_print = 1 ; 
						}
						r = r + 1;
					}
					if(val_print == 0)//si elle a pas deja été afficher (en rouge) alors on l'affiche
					fprintf(stdout," %d ;",tab_affichage[q]);
					val_print = 0;
					r = 0;
					q = q + 1;
				}
				q = 0 ;
				congrate = 1;
		break;

		case 2 : // mode INSTANTANER

			while(nbr_boules_restants  > 0 || step == 98)
				{
					efface_ecran();
			colonne = 0 ;
			ligne = 0 ;
			//affiche la grille
			while(ligne < 3)
			{
				while(colonne < 9)
					{
						if(tab[ligne][colonne] < 10)
							{
								fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
							}
						else fprintf(stdout,"| %d |",tab[ligne][colonne]);
					colonne = colonne + 1;
					
					}
				fprintf(stdout,"\n");
				colonne = 0; //reinitialise la variable c à 0
				ligne = ligne + 1;
			}//fin affichage grille

			
			fprintf(stdout,"\n");
			fprintf(stdout,"Les boules déjà tirées sont :");
			
			// affiche le tableau avec les boules tirées 
			while(q < p)
			{
				fprintf(stdout," %d ;",tab_affichage[q]);
				q = q + 1;
			}
			q = 0 ;

			fprintf(stdout,"\n");	
	
			//tire nombre aléatoire
			nbr_tirage = rand()%99+1;

			//test qu'il n'a pas déjà éte tirée
			while(tab_valeur[nbr_tirage - 1] == 0)
			{
				nbr_tirage = rand()%99+1;
			}
			tab_affichage[p] = nbr_tirage ; 
			p = p + 1;

			//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
			tab_valeur[nbr_tirage - 1] = 0;
	
			step = step + 1; //compte nbr boule sortie
			while(id_valeur < 15) // comptage boulles trouvée (comparaison des deux grilles)
			{	
				if( nbr_tirage == tab_grid[id_valeur] )
				{
					nbr_boules_restants = nbr_boules_restants - 1;
				}//if END
				id_valeur = id_valeur + 1;
			}		
			id_valeur = 0;	

			}
			congrate = 1;
		 break;

		 case 3 :
		 nbr_boules_restants = 0;
		 congrate = 0;
		 break;

		 default : 
		 //gère erreurs
		 break;
		 }//switch end

	} //while END 
	if(congrate == 1)
	{
	fprintf(stdout,"\nFélicitation\nVous avez réussi en %d \n", step);
	}else fprintf(stdout,"\nC'est fini\nA bientôt \n");

	return 0;
}// main END

