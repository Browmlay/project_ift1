	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>
	#include <time.h>
	#include "efface_ecran.h"

	void color_red () {
	printf("\033[1;31m");
	 
	}
	void color_reset () {
	printf("\033[0m");
	}

int main(int argc, char *argv[])
{

	efface_ecran();
	int tab[3][9];

	//fc time_init() : initialisation de la fonction rand 	
	time_t t1;
	(void)time(&t1);
	srand((long)t1);
 
     //fc create_random_nbrs() création des nombres aléatoires dans chaque case TOUS DIFFERENTS

	int i = 0;
	int bc3 = 0;
	int bc2 = 0;
	int bc1 = 0;
	while(i <= 2)
	{
		switch(i)
		{
			case 0 :
				
				while(bc2 <= 8)
				{

				tab[i][bc2] = bc2*10 + rand()%9+1; 
				bc2 = bc2 + 1;

				}
			break ;

			case 1 :
				
				while(bc1 <= 8)
				{

					tab[i][bc1] = bc1*10 + rand()%9+1; 

						while(tab[i][bc1] == tab[i-1][bc1])
						{
							tab[i][bc1] = bc1*10 + rand()%9+1; 
						}

					bc1 = bc1 + 1;

				}
			break;

			case 2 :
			
				while(bc3 <= 8)
				{

					tab[i][bc3] = bc3*10 + rand()%9+1; 
					
						while(tab[i][bc3] == tab[i-1][bc3] || tab[i][bc3] == tab[i-2][bc3])
						{
							tab[i][bc3] = bc3*10 + rand()%9+1; 
						}

					bc3 = bc3 + 1;

				}
			break;
		}
	i = i + 1;
	}
	
	//fc create_blank() : Met 4 case vide par ligne (aléatoire)
	int k = 0; 
	while(k <= 2)
	{
		int n1; 
		int n2;
		int n3; 
		int n4;
		n1 = rand()%8+1;
		n2 = rand()%8+1;
			while(n2 == n1)
			{
				n2 = rand()%8+1;
			}

		n3 = rand()%8+1;
			while(n3 == n1 || n3 == n2)
			{
				n3 = rand()%8+1;
			}
		n4 = rand()%8+1;
			while(n4 == n1 || n4 == n2 || n4 == n3)
			{
				n4 = rand()%8+1;
			}
		
		tab[k][n1] = 0;
		tab[k][n2] = 0;
		tab[k][n3] = 0;
		tab[k][n4] = 0;	
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;

		k = k + 1 ;
	}
	//fc show_grid() affiche la grille

	int c = 0 ;
	int l = 0 ;
	
	while(l <= 2)
	{
		while(c <= 8)
			{
				if(tab[l][c] < 10)
					{
						fprintf(stdout,"| 0%d |",tab[l][c]);
					}
				else fprintf(stdout,"| %d |",tab[l][c]);

			c = c + 1;

			}

		c = 0; //reinitialise la variable c à 0
		l = l + 1;

		fprintf(stdout,"\n");

	}
	
	//fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
	int c1 = 0;
	int l1 = 0;
	int w = 0;
	int tab_grid[15]; //15 nbr dans la grille (case vide enlever)
	while(l1 <= 2)
		{
			while(c1 <= 8)
			{
				if(tab[l1][c1] != 0)
				{
					tab_grid[w] = tab[l1][c1];
					w = w + 1;
				}
				c1 = c1 + 1;
			}
			c1 = 0;
			l1 = l1 + 1;
		}

					
	
	//fc tirage()  

	//sous fonction créé tabeau
	int val = 1 ;
	int tab_valeur[99];

	//cree un tableau avec des valeurs de 0 à 99
	while(val <= 99)
	{
		tab_valeur[val - 1] = val;
		val = val + 1;
	}


	int nbr_tirage = 0;
	int nbr_boules_restants = 15;
	int I = 0 ;
	int step = 0;
	int tab_affichage[100] ; // tableau d'affichage des nbrs tirées
	int p = 0 ;
	int q = 0 ;
	int r = 0 ;
	int val_print = 0 ;
	
	while(nbr_boules_restants > 0 || step >= 98) //pour etre sur de sortir de la boucle le step 98
	{
		
		fprintf(stdout,"\n");
		fprintf(stdout,"Les boules déjà tirées sont :");
		fprintf(stdout,"\n");		
		fprintf(stdout,"\nBoule tirées : %d     Boules restante à trouver : %d \n",step,nbr_boules_restants );
		fprintf(stdout,"1 pour continuer \n2 pour instantané(BETA_0.0.1) \n3 pour quitter \n");
		
		int key = 0;
		scanf("%d", &key);

		if(key == 1)//condition pour faire avancer le tirage
		{

			//tire nombre aléatoire
			nbr_tirage = rand()%99+1;

			//test qu'il n'a pas déjà éte tirée
			while(tab_valeur[nbr_tirage - 1] == 0)
			{
				nbr_tirage = rand()%99+1;
			}
			tab_affichage[p] = nbr_tirage ; 
			p = p + 1;

			//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
			tab_valeur[nbr_tirage - 1] = 0;
						
			
			step = step + 1; //compte nbr boule sortie
			while(I < 15) // comptage boulles trouvée (comparaison des deux grilles)
			{	
				if( nbr_tirage == tab_grid[I] )
				{
					nbr_boules_restants = nbr_boules_restants - 1;
				}//if END
				I = I + 1;
			}		
			I = 0;


		//AFFICHAGE
		//affiche la grille
		efface_ecran();
		c = 0 ;
		l = 0 ;
		while(l <= 2)
		{
			while(c <= 8)
				{
					if(tab[l][c] < 10)
						{
							fprintf(stdout,"| 0%d |",tab[l][c]);
						}
					else fprintf(stdout,"| %d |",tab[l][c]);
				c = c + 1;
				
				}
			fprintf(stdout,"\n");
			c = 0; //reinitialise la variable c à 0
			l = l + 1;
		}//fin affichage grille
		
		// AFFICHAGE boules sortie 
		while(q < p)
			{
				while(r < 15)
				{
					if(tab_affichage[q] == tab_grid[r])// afficher les nombres bons en rouge
					{
						color_red();
						fprintf(stdout," %d ;",tab_affichage[q]);
						color_reset();
						val_print = 1 ; 
					}
					r = r + 1;
				}
				if(val_print == 0)//si elle a pas deja été afficher (en rouge) alors on l'affiche
				fprintf(stdout," %d ;",tab_affichage[q]);
				val_print = 0;
				r = 0;
				q = q + 1;
			}
			q = 0 ;

		}else if(key == 2) // mode INSTANTANER
			{

			while(nbr_boules_restants  > 0 || step == 98)
				{
					efface_ecran();
		c = 0 ;
		l = 0 ;
		//affiche la grille
		while(l <= 2)
		{
			while(c <= 8)
				{
					if(tab[l][c] < 10)
						{
							fprintf(stdout,"| 0%d |",tab[l][c]);
						}
					else fprintf(stdout,"| %d |",tab[l][c]);
				c = c + 1;
				
				}
			fprintf(stdout,"\n");
			c = 0; //reinitialise la variable c à 0
			l = l + 1;
		}//fin affichage grille

		
		fprintf(stdout,"\n");
		fprintf(stdout,"Les boules déjà tirées sont :");
		
		// affiche le tableau avec les boules tirées 
		while(q < p)
		{
			fprintf(stdout," %d ;",tab_affichage[q]);
			q = q + 1;
		}
		q = 0 ;

		fprintf(stdout,"\n");
					
	
					//tire nombre aléatoire
					nbr_tirage = rand()%99+1;

					//test qu'il n'a pas déjà éte tirée
					while(tab_valeur[nbr_tirage - 1] == 0)
					{
						nbr_tirage = rand()%99+1;
					}
					tab_affichage[p] = nbr_tirage ; 
					p = p + 1;

					//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
					tab_valeur[nbr_tirage - 1] = 0;
								
					
					step = step + 1; //compte nbr boule sortie
					while(I < 15) // comptage boulles trouvée (comparaison des deux grilles)
					{	
						if( nbr_tirage == tab_grid[I] )
						{
							nbr_boules_restants = nbr_boules_restants - 1;
						}//if END
						I = I + 1;
					}		
					I = 0;	

				}
			}
		 
	} //while END 

	fprintf(stdout,"\nFélicitation\nVous avez réussi en %d \n", step);
	return 0;
}// main END
	