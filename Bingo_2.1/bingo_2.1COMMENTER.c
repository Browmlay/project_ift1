	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>
	#include <time.h>
	#include "efface_ecran.h"
	
	//*fonction changer de couleur en rouge
	void color_red () {
	printf("\033[1;31m"); 
	}

	//*fonction remetre en noir la couleur 
	void color_reset () {
	printf("\033[0m");
	}

//*Fonction principale
int main(int argc, char *argv[])
{

	efface_ecran(); //efface l'écran
	int tab[3][9]; //crée le tableau == grille

	//*fc time_init() : initialisation de la fonction rand 	
	time_t t1;
	(void)time(&t1);
	srand((long)t1);
 
     //*fc create_random_nbrs() création des nombres aléatoires dans chaque case TOUS DIFFERENTS

	//intinitialisastion des var utiliser
	int i = 0;
	int bc3 = 0;
	int bc2 = 0;
	int bc1 = 0;
	while(i <= 2)// i = 0 ,1, 2 == trois ligne 
	{
		switch(i)
		{
			case 0 : // première ligne
				
				while(bc2 <= 8) // les case de 0 a 8
				{

				tab[i][bc2] = bc2*10 + rand()%9+1; //met un rdm
				bc2 = bc2 + 1;

				}
			break ;

			case 1 ://deuxiemme ligne 
				
				while(bc1 <= 8)
				{

					tab[i][bc1] = bc1*10 + rand()%9+1; 

						while(tab[i][bc1] == tab[i-1][bc1])//met rdm dfferent de la 1er ligne 
						{
							tab[i][bc1] = bc1*10 + rand()%9+1; 
						}

					bc1 = bc1 + 1;

				}
			break;

			case 2 ://troisième ligne
			
				while(bc3 <= 8)
				{

					tab[i][bc3] = bc3*10 + rand()%9+1; 
					
						while(tab[i][bc3] == tab[i-1][bc3] || tab[i][bc3] == tab[i-2][bc3])])//met rdm dfferent de la 1er et 2eme ligne 
						{
							tab[i][bc3] = bc3*10 + rand()%9+1; 
						}

					bc3 = bc3 + 1;

				}
			break;
		}
	i = i + 1;//chnage de ligne
	}
	
	//*fc create_blank() : Met 4 case vide par ligne (aléatoire)
	int k = 0; //var_itère
	while(k <= 2)
	{
		int n1; //case vide numéro 1
		int n2;//case vide numéro 2
		int n3; //case vide numéro 3
		int n4;//case vide numéro 4
		n1 = rand()%8+1;//la case vide a la première ligne sera la = rand 
		n2 = rand()%8+1;
			while(n2 == n1)// la deuxième de ligne sera rand mais différent de la première case vide
			{
				n2 = rand()%8+1;
			}

		n3 = rand()%8+1;
			while(n3 == n1 || n3 == n2)//etc..
			{
				n3 = rand()%8+1;
			}
		n4 = rand()%8+1;
			while(n4 == n1 || n4 == n2 || n4 == n3)
			{
				n4 = rand()%8+1;
			}
		
		tab[k][n1] = 0;//on remplace dans le tableau les troues (0)
		tab[k][n2] = 0;
		tab[k][n3] = 0;
		tab[k][n4] = 0;	
		n1 = 0;//rénitialise les varibales 
		n2 = 0;
		n3 = 0;
		n4 = 0;

		k = k + 1 ;//incrémente boucle
	}

	//*fc show_grid() affiche la grille

	int c = 0 ;
	int l = 0 ;	
	while(l <= 2)
	{
		while(c <= 8)
			{
				if(tab[l][c] < 10)
					{
						fprintf(stdout,"| 0%d |",tab[l][c]);
					}
				else fprintf(stdout,"| %d |",tab[l][c]);

			c = c + 1;

			}
		c = 0; //reinitialise la variable c à 0
		l = l + 1;

		fprintf(stdout,"\n");

	}
	
	//fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
	int c1 = 0;
	int l1 = 0;
	int w = 0;
	int tab_grid[15]; //15 nbr dans la grille (case vide enlever)
	while(l1 <= 2)
		{
			while(c1 <= 8)
			{
				if(tab[l1][c1] != 0)//on prend seulement les case qui sont pas des 0, pour les mettres dans le tableau réponse 
				{
					tab_grid[w] = tab[l1][c1];
					w = w + 1;//la prochaine case sera remplie
				}
				c1 = c1 + 1;
			}
			c1 = 0;
			l1 = l1 + 1;
		}

					
	
	//*fc tirage()  

	//initialisatin des variables utilisée dans la fc tirage
	int val = 1 ;//utiliser création du tableau 
	int tab_valeur[100];//les nbr pouvant être tirée sont de 0 à 99 -> 100éléments  
	int nbr_tirage = 0;//le dern nbr tiré stockée dedans  
	int nbr_boules_restants = 15;//compte nbr de boules redstante 
	int step = 0;//compte le nombre de boules tirées
	int tab_affichage[100] ; // tableau d'affichage des nbrs tirées
	int val_print = 0; //booléne pour savoir si la valeur à deja ete afficher 
	int congrate = 0;//var pour choisir la réponse à afficher a la fin du prgm
	int key = 0;//var du MENU pour seléctionner mode de jeu
	int I = 0 ;//var incrémentation
	int p = 0 ;//var incrémentation
	int q = 0 ;//var incrémentation
	int r = 0 ;//var incrémentation

	//**sous fonction créé tabeau
	//cree un tableau avec des valeurs de 0 à 99
	while(val <= 99)
	{
		tab_valeur[val - 1] = val;//on a un tableau : {1;2;3;[...];99}
		val = val + 1;
	}

	while(nbr_boules_restants > 0 || step >= 98) //pour etre sur de sortir de la boucle le step 99( toutes les boules tirées)
	{
		//interface utilisateur 
		fprintf(stdout,"\n");
		fprintf(stdout,"Les boules déjà tirées sont :");
		fprintf(stdout,"\n");		
		fprintf(stdout,"\nBoule tirées : %d     Boules restante à trouver : %d \n",step,nbr_boules_restants );
		fprintf(stdout,"1 pour continuer \n2 pour instantané(BETA_0.0.1) \n3 pour quitter \n");
	
		scanf("%d", &key);//lit le choit de l'utilisateur, 1, 2 ou 3

		switch(key)
		{
			case 1 ://Tirée une boule

				//tire nombre aléatoire
				nbr_tirage = rand()%99+1;

				//test qu'il n'a pas déjà éte tirée
				while(tab_valeur[nbr_tirage - 1] == 0)
				{
					nbr_tirage = rand()%99+1;
				}
				tab_affichage[p] = nbr_tirage ; 
				p = p + 1;

				//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
				tab_valeur[nbr_tirage - 1] = 0;
							
				
				step = step + 1; //compte nbr boule sortie
				while(I < 15) // comptage boulles trouvée (comparaison des deux grilles)
				{	
					if( nbr_tirage == tab_grid[I] )
					{
						nbr_boules_restants = nbr_boules_restants - 1;
					}//if END
					I = I + 1;
				}		
				I = 0;


			//AFFICHAGE

			//affiche la grille
			efface_ecran(); //efface l'écran
			c = 0 ;
			l = 0 ;
			while(l <= 2)
			{
				while(c <= 8)
					{
						if(tab[l][c] < 10)
							{
								fprintf(stdout,"| 0%d |",tab[l][c]);
							}
						else fprintf(stdout,"| %d |",tab[l][c]);
					c = c + 1;
					
					}
				fprintf(stdout,"\n");
				c = 0; //reinitialise la variable c à 0
				l = l + 1;
			}//fin affichage grille
			
			// AFFICHAGE boules sortie 
			while(q < p)
				{
					while(r < 15)
					{
						if(tab_affichage[q] == tab_grid[r])// afficher les nombres bons en rouge
						{
							color_red();
							fprintf(stdout," %d ;",tab_affichage[q]);
							color_reset();
							val_print = 1 ; 
						}
						r = r + 1;
					}
					if(val_print == 0)//si elle a pas deja été afficher (en rouge) alors on l'affiche
					fprintf(stdout," %d ;",tab_affichage[q]);
					val_print = 0;
					r = 0;
					q = q + 1;
				}
				q = 0 ;
				congrate = 1;//choisit le bon message de fin
		break;

		case 2 : // mode INSTANTANER

			while(nbr_boules_restants  > 0 || step == 98)
				{
			efface_ecran();
			c = 0 ;
			l = 0 ;
			//affiche la grille
			while(l <= 2)
			{
				while(c <= 8)
					{
						if(tab[l][c] < 10)
							{
								fprintf(stdout,"| 0%d |",tab[l][c]);
							}
						else fprintf(stdout,"| %d |",tab[l][c]);
					c = c + 1;
					
					}
				fprintf(stdout,"\n");
				c = 0; //reinitialise la variable c à 0
				l = l + 1;
			}//fin affichage grille

			
			fprintf(stdout,"\n");
			fprintf(stdout,"Les boules déjà tirées sont :");
			
			// affiche le tableau avec les boules tirées 
			while(q < p)
			{
				fprintf(stdout," %d ;",tab_affichage[q]);
				q = q + 1;
			}
			q = 0 ;

			fprintf(stdout,"\n");	
	
			//tire nombre aléatoire
			nbr_tirage = rand()%99+1;

			//test qu'il n'a pas déjà éte tirée
			while(tab_valeur[nbr_tirage - 1] == 0)
			{
				nbr_tirage = rand()%99+1;
			}
			tab_affichage[p] = nbr_tirage ; 
			p = p + 1;

			//le nombre tiree dans le tableau vas etre remplacer par un zero pour ne plus etre choisit
			tab_valeur[nbr_tirage - 1] = 0;
						
			
			step = step + 1; //compte nbr boule sortie
			while(I < 15) // comptage boulles trouvée (comparaison des deux grilles)
			{	
				if( nbr_tirage == tab_grid[I] )
				{
					nbr_boules_restants = nbr_boules_restants - 1;
				}//if END
				I = I + 1;
			}		
			I = 0;	

			}
			congrate = 1;//affiche bon message de fin
		 break;

		 case 3 ://si il veut quitter
		 nbr_boules_restants = 0; //arrète la boucle 
		 congrate = 0;//affiche a bientot
		 break;
		 }//switch end
	} //while END 

	if(congrate == 1)//si le joueur a fini,félicitation
	{
	fprintf(stdout,"\nFélicitation\nVous avez réussi en %d \n", step);
	}else fprintf(stdout,"\nC'est fini\nA bientôt \n");//si non en revoir 

	return 0;
}// main END
	