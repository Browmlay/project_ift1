#include<stdio.h>
#include<stdlib.h>
#include"affichage_menu.h"
#include <math.h>
#include <time.h>
#include "saisie.h" //copyright Eric BACHARD
#include "efface_ecran.h"


typedef enum NumeroLigne
{
    PREMIERE_LIGNE  = 0,
    DEUXIEME_LIGNE  = 1,
    TROISIEME_LIGNE = 2
} NUMERO_LIGNE;


// declaration forward
void color_red(void);
void color_reset(void);


//internal functions
void color_red ()
{
    fprintf(stdout, "\033[1;31m");
}



void color_reset ()
{
    fprintf(stdout, "\033[0m");
}





int main(void)

{
efface_ecran();
int exit = 0;

while( exit != 1)
{
affichage_menu();
int choix_menu;
scanf("%d",&choix_menu);
	switch(choix_menu)
		{
			case 1 : 
				efface_ecran();
				fprintf(stdout,"vous etes sur votre profil \n");
				fprintf(stdout,"Votre nom est :  \n");
				fprintf(stdout,"Votre prénom est :  me semble-t-il ?\n");
				fprintf(stdout,"Vos stats : \n \n");
				fprintf(stdout,"Vous avez joués  parties \n"); //nombre de parties
				fprintf(stdout,"Votre meilleur score est de //x itérations \n");
				fprintf(stdout,"//x temps c'est la grille la plus rapide que vous avez faite et vous l'avez faite en //x itérations");
				fprintf(stdout,"Contacter notre chef de projet Nicolas Saintvoirin à l'adresse suivante : \n nicolas.saintvoirin@utbm.fr\n\n");
                	


				break;
			case 2 : 
				efface_ecran();
				fprintf(stdout,"vous etes sur votre espace de jeu \n"); 
				// Dans le cas où on ait plusieurs jeux (permettra affichage des jeux de la plateforme)
	

				break;
			case 3 : 
				fprintf(stdout,"vous quittez la plateforme\n");
				fprintf(stdout,"etes vous sur de vouloir quitter \n");
				exit = 1;
				break;
			
			
			case 4 : 
				efface_ecran();
				fprintf(stdout,"vous etes sur le jeu du bingo\n");


				int b_exit = 1;
				do{

				    int colonne = 0;
				    efface_ecran();
				    int tab[3][9];

				    //fc time_init() : initialisation de la fonction rand 
				    time_t t1;
				    (void)time(&t1);
				    srand((long)t1);

				    //fc create_random_nbrs() création des nombres aléatoires dans chaque case TOUS DIFFERENTS

				    NUMERO_LIGNE ligne = PREMIERE_LIGNE;


				    for (ligne = 0 ; ligne < 3; ligne++)
				    {

					switch(ligne)
					{
					    case PREMIERE_LIGNE :
						for (colonne = 0 ; colonne < 9 ; colonne++)
						{
						    tab[PREMIERE_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
						}
					    break ;

					    case DEUXIEME_LIGNE :
						for (colonne = 0 ; colonne < 9 ; colonne++)
						{
						    tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

						    while(tab[DEUXIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
						    {
							tab[DEUXIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
						    }
						}
					    break;

					    case TROISIEME_LIGNE:

						for (colonne = 0 ; colonne < 9 ; colonne++)
						{
						    tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 

						    while(tab[TROISIEME_LIGNE][colonne] == tab[DEUXIEME_LIGNE][colonne] || tab[TROISIEME_LIGNE][colonne] == tab[PREMIERE_LIGNE][colonne])
						    {
							tab[TROISIEME_LIGNE][colonne] = (colonne * 10) + rand()%9+1; 
						    }
						}
					    break;

					    default:
					    break;
					}
				    }

					
					//fc create_blank() : Met 4 cases vide par ligne (aléatoire)
					ligne = 0;
					for (ligne = 0 ; ligne < 3; ligne ++)
					
					{
						int case_vide1 = 0;
						int case_vide2 = 0;
						int case_vide3 = 0;
						int case_vide4 = 0;
						case_vide1 = rand()%8+1;
						case_vide2 = rand()%8+1;
							while(case_vide2 == case_vide1)
							{
								case_vide2 = rand()%8+1;
							}

						case_vide3 = rand()%8+1;
							while(case_vide3 == case_vide1 || case_vide3 == case_vide2)
							{
								case_vide3 = rand()%8+1;
							}
						case_vide4 = rand()%8+1;
							while(case_vide4 == case_vide1 || case_vide4 == case_vide2 || case_vide4 == case_vide3)
							{
								case_vide4 = rand()%8+1;
							}
						
						tab[ligne][case_vide1] = 0;
						tab[ligne][case_vide2] = 0;
						tab[ligne][case_vide3] = 0;
						tab[ligne][case_vide4] = 0;
						case_vide1 = 0;
						case_vide2 = 0;
						case_vide3 = 0;
						case_vide4 = 0;

					}
					//fc show_grid() affiche la grille

					 colonne = 0 ;
					 ligne = 0 ;
					
					while(ligne < 3)
					{
						while(colonne < 9)
							{
								if(tab[ligne][colonne] < 10)
									{
										fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
									}
								else fprintf(stdout,"| %d |",tab[ligne][colonne]);

							colonne = colonne + 1;

							}

						colonne = 0; //reinitialise la variable c à 0
						ligne = ligne + 1;

						fprintf(stdout,"\n");

					}
					
					//fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
					colonne = 0;
					ligne = 0;
					int vlr_liste = 0;
					int tab_grid[15]; //15 nbr dans la grille (case vide enlever)
					while(ligne < 3)
						{
							while(colonne < 9)
							{
								if(tab[ligne][colonne] != 0)
								{
									tab_grid[vlr_liste] = tab[ligne][colonne];
									vlr_liste = vlr_liste + 1;
								}
								colonne = colonne + 1;
							}
							colonne = 0;
							ligne = ligne + 1;
						}




					//fonction créer liste valeurs à trouver
					int val = 1 ;
					int liste_valeurs_a_trouver[100];

					//créer un tableau avec des valeurs de 0 à 99
					for (val = 0 ; val < 100 ; val ++)
					{
						liste_valeurs_a_trouver[val - 1] = val;
						
					}
					//fc tirage
					int nbr_tire = 0;
					int nbr_boules_restantes_a_trouver = 15;
					int id_valeur = 0;
					int etape_tirage = 0;
					int tab_affichage[100]; // tableau d'affichage des nbrs tirés
					int p = 0;
					int q = 0;
					int r = 0;
					int val_print = 0;
					int congrate = 0;
					
					while(nbr_boules_restantes_a_trouver > 0 || etape_tirage > 99) //pour être sur de sortir de la boucle à la step 98 ou quand il n'y a plus de boules à trouver 
					{
						
						fprintf(stdout,"\n");
						fprintf(stdout,"Les boules déjà tirées sont :");
						fprintf(stdout,"\n");
						fprintf(stdout,"\nBoules tirées : %d     Boules restantes à trouver : %d \n",etape_tirage,nbr_boules_restantes_a_trouver );
						fprintf(stdout,"1 pour continuer \n2 pour instantané(BETA_0.0.1) \n3 pour quitter \n");
						
						int key = 0;
						key = saisie_nombre_entier_court(1,3);

						switch(key) //condition pour faire avancer le tirage
						{
						case 1 :

							//tire nombre aléatoire
								nbr_tire = rand()%99+1;

								//test qu'il n'a pas déja été tiré
								while(liste_valeurs_a_trouver[nbr_tire - 1] == 0)
								{
									nbr_tire = rand()%99+1;
								}
								tab_affichage[p] = nbr_tire ;
								p = p + 1;

								//le nombre tiré dans le tableau va être remplacer par un zéro pour ne plus être choisi
								liste_valeurs_a_trouver[nbr_tire - 1] = 0;


								etape_tirage = etape_tirage + 1; //compte nbr boules sorties
								while(id_valeur < 15) // comptage boules trouvées (comparaison des deux grilles)
								{
									if( nbr_tire == tab_grid[id_valeur] )
									{
										nbr_boules_restantes_a_trouver = nbr_boules_restantes_a_trouver - 1;
									}//if END
									id_valeur = id_valeur + 1;
								}
								id_valeur = 0;


							//AFFICHAGE
							//affiche la grille
							efface_ecran();
							colonne = 0 ;
							ligne = 0 ;
							while(ligne < 3)
							{
								while(colonne < 9)
									{
										if(tab[ligne][colonne] < 10)
											{
												fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
											}
										else fprintf(stdout,"| %d |",tab[ligne][colonne]);
									colonne = colonne + 1;

									}
								fprintf(stdout,"\n");
								colonne = 0; //reinitialise la variable colonne à 0
								ligne = ligne + 1;
							}//fin affichage grille

							// AFFICHAGE boules sorties 
							while(q < p)
								{
									while(r < 15)
									{
										if(tab_affichage[q] == tab_grid[r])// afficher les nombres bons en rouge
										{
											color_red();
											fprintf(stdout," %d ;",tab_affichage[q]);
											color_reset();
											val_print = 1 ;
										}
										r = r + 1;
									}
									if(val_print == 0)//si elle a pas deja été afficher (en rouge) alors on l'affiche
									fprintf(stdout," %d ;",tab_affichage[q]);
									val_print = 0;
									r = 0;
									q = q + 1;
								}
								q = 0 ;
								congrate = 1;
						break;

						case 2 : // mode INSTANTANER

							while(nbr_boules_restantes_a_trouver > 0 || etape_tirage == 98) 
								{
									efface_ecran();
									colonne = 0 ;
									ligne = 0 ;
									//affiche la grille
									while(ligne < 3)
									{
									    while(colonne < 9)
									    {
										if(tab[ligne][colonne] < 10)
											{
												fprintf(stdout,"| 0%d |",tab[ligne][colonne]);
											}
										else fprintf(stdout,"| %d |",tab[ligne][colonne]);
									colonne = colonne + 1;

									}
								fprintf(stdout,"\n");
								colonne = 0; //reinitialise la variable colonnne à 0
								ligne = ligne + 1;
							}//fin affichage grille
							fprintf(stdout,"\n");
							fprintf(stdout,"Les boules déjà tirées sont :");

							// affiche le tableau avec les boules tirées 
							while(q < p)
							{
								fprintf(stdout," %d ;",tab_affichage[q]);
								q = q + 1;
							}
							q = 0 ;
							fprintf(stdout,"\n");

							//tire nombre aléatoire
							nbr_tire = rand()%99+1;

							//test qu'il n'a pas déjà éte tiré
							while(liste_valeurs_a_trouver[nbr_tire - 1] == 0)
							{
								nbr_tire = rand()%99+1;
							}
							tab_affichage[p] = nbr_tire ;
							p = p + 1;

							//le nombre tiré dans le tableau va être remplacé par un zero pour ne plus être choisi
							liste_valeurs_a_trouver[nbr_tire - 1] = 0;
							etape_tirage = etape_tirage + 1; //compte nbr boules sorties
							while(id_valeur < 15) // comptage boules trouvées (comparaison des deux grilles)
							{	
								if( nbr_tire == tab_grid[id_valeur] )
								{
									nbr_boules_restantes_a_trouver = nbr_boules_restantes_a_trouver - 1;
								}//if END
								id_valeur = id_valeur + 1;
							}
							id_valeur = 0;	

							}
							congrate = 1;
						 break;

						 case 3 :
						 nbr_boules_restantes_a_trouver = 0;
						 congrate = 0;
						 b_exit = 2;
						 break;

						 default :
						 //gère erreurs
						 break;
						 }//switch end

					} //while END
					if(congrate == 1)
					{
					fprintf(stdout,"\nFélicitation\nVous avez réussi en %d tirages \n", etape_tirage);
					}else fprintf(stdout,"\nC'est fini\nA bientôt \n");
					if(b_exit != 2)
						{
						fprintf(stdout, "1 - REJOUER\n2 - QUITTER\n");
						b_exit = saisie_nombre_entier_court(1,2);
					}

				}while(b_exit == 1);

					if(b_exit != 2)
					fprintf(stdout,"\nC'est fini\nA bientôt \n");
				


				break;



				
				//case 5 : 
					//fprintf(stdout,"vous êtes sur le jeu de la bataille navale");
					//break;
			default:
				fprintf(stdout,"case default \n"); 
					
		}
}//if end                        
return EXIT_SUCCESS;
}



