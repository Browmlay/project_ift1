// dear imgui: standalone example application for SDL2 + OpenGL
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (SDL is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no standard header to access modern OpenGL functions easily. Alternatives are GLEW, Glad, etc.)


#include <SDL.h>
#include "definition.h"
#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>

#include <GL/gl3w.h>
#include <GL/gl.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <math.h>
#include <time.h>
#include "saisie.h" //copyright Eric BACHARD






GLuint textureID0 = 0; 
    GLuint textureID1 = 1; 
    GLuint textureID2 = 2; 
    GLuint textureID3 = 3; 
    GLuint textureID4 = 4; 
    GLuint textureID5 = 5; 
    GLuint textureID6 = 6; 
    GLuint textureID7 = 7; 
    GLuint textureID8 = 8; 
    GLuint textureID9 = 9; 
    GLuint textureID10 = 10;
    GLuint textureID11 = 11;
    GLuint textureID12 = 12;
    GLuint textureID13 = 13;
    GLuint textureID14 = 14;
    GLuint textureID15 = 15;
    GLuint textureID16 = 16;
    GLuint textureID17 = 17;
    GLuint textureID18 = 18;
    GLuint textureID19 = 19;
    GLuint textureID20 = 20;
    GLuint textureID21 = 21;
    GLuint textureID22 = 22;
    GLuint textureID23 = 23;
    GLuint textureID24 = 24;
    GLuint textureID25 = 25;
    GLuint textureID26 = 26;
    GLuint textureID27 = 27;
    GLuint textureID28 = 28;
    GLuint textureID29 = 29;
    GLuint textureID30 = 30;
    GLuint textureID31 = 31;
    GLuint textureID32 = 32;
    GLuint textureID33 = 33;
    GLuint textureID34 = 34;
    GLuint textureID35 = 35;
    GLuint textureID36 = 36;
    GLuint textureID37 = 37;
    GLuint textureID38 = 38;
    GLuint textureID39 = 39;
    GLuint textureID40 = 40;
    GLuint textureID41 = 41;
    GLuint textureID42 = 42;
    GLuint textureID43 = 43;
    GLuint textureID44 = 44;
    GLuint textureID45 = 45;
    GLuint textureID46 = 46;
    GLuint textureID47 = 47;
    GLuint textureID48 = 48;
    GLuint textureID49 = 49;
    GLuint textureID50 = 50;
    GLuint textureID51 = 51;
    GLuint textureID52 = 52;
    GLuint textureID53 = 53;
    GLuint textureID54 = 54;
    GLuint textureID55 = 55;
    GLuint textureID56 = 56;
    GLuint textureID57 = 57;
    GLuint textureID58 = 58;
    GLuint textureID59 = 59;
    GLuint textureID60 = 60;
    GLuint textureID61 = 61;
    GLuint textureID62 = 62;
    GLuint textureID63 = 63;
    GLuint textureID64 = 64;
    GLuint textureID65 = 65;
    GLuint textureID66 = 66;
    GLuint textureID67 = 67;
    GLuint textureID68 = 68;
    GLuint textureID69 = 69;
    GLuint textureID70 = 70;
    GLuint textureID71 = 71;
    GLuint textureID72 = 72;
    GLuint textureID73 = 73;
    GLuint textureID74 = 74;
    GLuint textureID75 = 75;
    GLuint textureID76 = 76;
    GLuint textureID77 = 77;
    GLuint textureID78 = 78;
    GLuint textureID79 = 79;
    GLuint textureID80 = 80;
    GLuint textureID81 = 81;
    GLuint textureID82 = 82;
    GLuint textureID83 = 83;
    GLuint textureID84 = 84;
    GLuint textureID85 = 85;
    GLuint textureID86 = 86;
    GLuint textureID87 = 87;
    GLuint textureID88 = 88;
    GLuint textureID89 = 89;
    GLuint textureID90 = 90;
    GLuint textureID91 = 91;
    GLuint textureID92 = 92;
    GLuint textureID93 = 93;
    GLuint textureID94 = 94;
    GLuint textureID95 = 95;
    GLuint textureID96 = 96;
    GLuint textureID97 = 97;
    GLuint textureID98 = 98;
    GLuint textureID99 = 99;


int my_image_height = 100;
int my_image_width = 100;

// Simple helper function to load an image into a OpenGL texture with common settings
// https://github.com/ocornut/imgui/wiki/Image-Loading-and-Displaying-Examples

static bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height)
{
    // Load from file
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width = image_width;
    *out_height = image_height;

    return true;
}


//link to secondarys functions used in the main.cpp



// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif



// Main code
int main(void)
{
/***********************************/
srand(time(NULL));
int tab[3][9];
int ligne = 0;

   
 
/***********************************/

    // Setup SDL
    // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
    // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }

    // Decide GL+GLSL versions
#if __APPLE__
    // GL 3.2 Core + GLSL 150
    const char* glsl_version = "#version 150";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Window* window = SDL_CreateWindow("Dear ImGui SDL2+OpenGL3 example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'misc/fonts/README.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //    io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    io.Fonts->AddFontFromFileTTF("../fonts/DroidSans.ttf", 18.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool img0  = LoadTextureFromFile(IMAGE_PATH0, &textureID0, &my_image_width, &my_image_height);
    bool img1  = LoadTextureFromFile(IMAGE_PATH1, &textureID1, &my_image_width, &my_image_height);
    bool img2  = LoadTextureFromFile(IMAGE_PATH2, &textureID2, &my_image_width, &my_image_height);
    bool img3  = LoadTextureFromFile(IMAGE_PATH3, &textureID3, &my_image_width, &my_image_height);
    bool img4  = LoadTextureFromFile(IMAGE_PATH4, &textureID4, &my_image_width, &my_image_height);
    bool img5  = LoadTextureFromFile(IMAGE_PATH5, &textureID5, &my_image_width, &my_image_height);
    bool img6  = LoadTextureFromFile(IMAGE_PATH6, &textureID6, &my_image_width, &my_image_height);
    bool img7  = LoadTextureFromFile(IMAGE_PATH7, &textureID7, &my_image_width, &my_image_height);
    bool img8  = LoadTextureFromFile(IMAGE_PATH8, &textureID8, &my_image_width, &my_image_height);
    bool img9  = LoadTextureFromFile(IMAGE_PATH9, &textureID9, &my_image_width, &my_image_height);
    bool img10  = LoadTextureFromFile(IMAGE_PATH10, &textureID10, &my_image_width, &my_image_height);
    bool img11  = LoadTextureFromFile(IMAGE_PATH11, &textureID11, &my_image_width, &my_image_height);
    bool img12  = LoadTextureFromFile(IMAGE_PATH12, &textureID12, &my_image_width, &my_image_height);
    bool img13  = LoadTextureFromFile(IMAGE_PATH13, &textureID13, &my_image_width, &my_image_height);
    bool img14  = LoadTextureFromFile(IMAGE_PATH14, &textureID14, &my_image_width, &my_image_height);
    bool img15  = LoadTextureFromFile(IMAGE_PATH15, &textureID15, &my_image_width, &my_image_height);
    bool img16  = LoadTextureFromFile(IMAGE_PATH16, &textureID16, &my_image_width, &my_image_height);
    bool img17  = LoadTextureFromFile(IMAGE_PATH17, &textureID17, &my_image_width, &my_image_height);
    bool img18  = LoadTextureFromFile(IMAGE_PATH18, &textureID18, &my_image_width, &my_image_height);
    bool img19  = LoadTextureFromFile(IMAGE_PATH19, &textureID19, &my_image_width, &my_image_height);
    bool img20  = LoadTextureFromFile(IMAGE_PATH20, &textureID20, &my_image_width, &my_image_height);
    bool img21  = LoadTextureFromFile(IMAGE_PATH21, &textureID21, &my_image_width, &my_image_height);
    bool img22  = LoadTextureFromFile(IMAGE_PATH22, &textureID22, &my_image_width, &my_image_height);
    bool img23  = LoadTextureFromFile(IMAGE_PATH23, &textureID23, &my_image_width, &my_image_height);
    bool img24  = LoadTextureFromFile(IMAGE_PATH24, &textureID24, &my_image_width, &my_image_height);
    bool img25  = LoadTextureFromFile(IMAGE_PATH25, &textureID25, &my_image_width, &my_image_height);
    bool img26  = LoadTextureFromFile(IMAGE_PATH26, &textureID26, &my_image_width, &my_image_height);
    bool img27  = LoadTextureFromFile(IMAGE_PATH27, &textureID27, &my_image_width, &my_image_height);
    bool img28  = LoadTextureFromFile(IMAGE_PATH28, &textureID28, &my_image_width, &my_image_height);
    bool img29  = LoadTextureFromFile(IMAGE_PATH29, &textureID29, &my_image_width, &my_image_height);
    bool img30  = LoadTextureFromFile(IMAGE_PATH30, &textureID30, &my_image_width, &my_image_height);
    bool img31  = LoadTextureFromFile(IMAGE_PATH31, &textureID31, &my_image_width, &my_image_height);
    bool img32  = LoadTextureFromFile(IMAGE_PATH32, &textureID32, &my_image_width, &my_image_height);
    bool img33  = LoadTextureFromFile(IMAGE_PATH33, &textureID33, &my_image_width, &my_image_height);
    bool img34  = LoadTextureFromFile(IMAGE_PATH34, &textureID34, &my_image_width, &my_image_height);
    bool img35  = LoadTextureFromFile(IMAGE_PATH35, &textureID35, &my_image_width, &my_image_height);
    bool img36  = LoadTextureFromFile(IMAGE_PATH36, &textureID36, &my_image_width, &my_image_height);
    bool img37  = LoadTextureFromFile(IMAGE_PATH37, &textureID37, &my_image_width, &my_image_height);
    bool img38  = LoadTextureFromFile(IMAGE_PATH38, &textureID38, &my_image_width, &my_image_height);
    bool img39  = LoadTextureFromFile(IMAGE_PATH39, &textureID39, &my_image_width, &my_image_height);
    bool img40  = LoadTextureFromFile(IMAGE_PATH40, &textureID40, &my_image_width, &my_image_height);
    bool img41  = LoadTextureFromFile(IMAGE_PATH41, &textureID41, &my_image_width, &my_image_height);
    bool img42  = LoadTextureFromFile(IMAGE_PATH42, &textureID42, &my_image_width, &my_image_height);
    bool img43  = LoadTextureFromFile(IMAGE_PATH43, &textureID43, &my_image_width, &my_image_height);
    bool img44  = LoadTextureFromFile(IMAGE_PATH44, &textureID44, &my_image_width, &my_image_height);
    bool img45  = LoadTextureFromFile(IMAGE_PATH45, &textureID45, &my_image_width, &my_image_height);
    bool img46  = LoadTextureFromFile(IMAGE_PATH46, &textureID46, &my_image_width, &my_image_height);
    bool img47  = LoadTextureFromFile(IMAGE_PATH47, &textureID47, &my_image_width, &my_image_height);
    bool img48  = LoadTextureFromFile(IMAGE_PATH48, &textureID48, &my_image_width, &my_image_height);
    bool img49  = LoadTextureFromFile(IMAGE_PATH49, &textureID49, &my_image_width, &my_image_height);
    bool img50  = LoadTextureFromFile(IMAGE_PATH50, &textureID50, &my_image_width, &my_image_height);
    bool img51  = LoadTextureFromFile(IMAGE_PATH51, &textureID51, &my_image_width, &my_image_height);
    bool img52  = LoadTextureFromFile(IMAGE_PATH52, &textureID52, &my_image_width, &my_image_height);
    bool img53  = LoadTextureFromFile(IMAGE_PATH53, &textureID53, &my_image_width, &my_image_height);
    bool img54  = LoadTextureFromFile(IMAGE_PATH54, &textureID54, &my_image_width, &my_image_height);
    bool img55  = LoadTextureFromFile(IMAGE_PATH55, &textureID55, &my_image_width, &my_image_height);
    bool img56  = LoadTextureFromFile(IMAGE_PATH56, &textureID56, &my_image_width, &my_image_height);
    bool img57  = LoadTextureFromFile(IMAGE_PATH57, &textureID57, &my_image_width, &my_image_height);
    bool img58  = LoadTextureFromFile(IMAGE_PATH58, &textureID58, &my_image_width, &my_image_height);
    bool img59  = LoadTextureFromFile(IMAGE_PATH59, &textureID59, &my_image_width, &my_image_height);
    bool img60  = LoadTextureFromFile(IMAGE_PATH60, &textureID60, &my_image_width, &my_image_height);
    bool img61  = LoadTextureFromFile(IMAGE_PATH61, &textureID61, &my_image_width, &my_image_height);
    bool img62  = LoadTextureFromFile(IMAGE_PATH62, &textureID62, &my_image_width, &my_image_height);
    bool img63  = LoadTextureFromFile(IMAGE_PATH63, &textureID63, &my_image_width, &my_image_height);
    bool img64  = LoadTextureFromFile(IMAGE_PATH64, &textureID64, &my_image_width, &my_image_height);
    bool img65  = LoadTextureFromFile(IMAGE_PATH65, &textureID65, &my_image_width, &my_image_height);
    bool img66  = LoadTextureFromFile(IMAGE_PATH66, &textureID66, &my_image_width, &my_image_height);
    bool img67  = LoadTextureFromFile(IMAGE_PATH67, &textureID67, &my_image_width, &my_image_height);
    bool img68  = LoadTextureFromFile(IMAGE_PATH68, &textureID68, &my_image_width, &my_image_height);
    bool img69  = LoadTextureFromFile(IMAGE_PATH69, &textureID69, &my_image_width, &my_image_height);
    bool img70  = LoadTextureFromFile(IMAGE_PATH70, &textureID70, &my_image_width, &my_image_height);
    bool img71  = LoadTextureFromFile(IMAGE_PATH71, &textureID71, &my_image_width, &my_image_height);
    bool img72  = LoadTextureFromFile(IMAGE_PATH72, &textureID72, &my_image_width, &my_image_height);
    bool img73  = LoadTextureFromFile(IMAGE_PATH73, &textureID73, &my_image_width, &my_image_height);
    bool img74  = LoadTextureFromFile(IMAGE_PATH74, &textureID74, &my_image_width, &my_image_height);
    bool img75  = LoadTextureFromFile(IMAGE_PATH75, &textureID75, &my_image_width, &my_image_height);
    bool img76  = LoadTextureFromFile(IMAGE_PATH76, &textureID76, &my_image_width, &my_image_height);
    bool img77  = LoadTextureFromFile(IMAGE_PATH77, &textureID77, &my_image_width, &my_image_height);
    bool img78  = LoadTextureFromFile(IMAGE_PATH78, &textureID78, &my_image_width, &my_image_height);
    bool img79  = LoadTextureFromFile(IMAGE_PATH79, &textureID79, &my_image_width, &my_image_height);
    bool img80  = LoadTextureFromFile(IMAGE_PATH80, &textureID80, &my_image_width, &my_image_height);
    bool img81  = LoadTextureFromFile(IMAGE_PATH81, &textureID81, &my_image_width, &my_image_height);
    bool img82  = LoadTextureFromFile(IMAGE_PATH82, &textureID82, &my_image_width, &my_image_height);
    bool img83  = LoadTextureFromFile(IMAGE_PATH83, &textureID83, &my_image_width, &my_image_height);
    bool img84  = LoadTextureFromFile(IMAGE_PATH84, &textureID84, &my_image_width, &my_image_height);
    bool img85  = LoadTextureFromFile(IMAGE_PATH85, &textureID85, &my_image_width, &my_image_height);
    bool img86  = LoadTextureFromFile(IMAGE_PATH86, &textureID86, &my_image_width, &my_image_height);
    bool img87  = LoadTextureFromFile(IMAGE_PATH87, &textureID87, &my_image_width, &my_image_height);
    bool img88  = LoadTextureFromFile(IMAGE_PATH88, &textureID88, &my_image_width, &my_image_height);
    bool img89  = LoadTextureFromFile(IMAGE_PATH89, &textureID89, &my_image_width, &my_image_height);
    bool img90  = LoadTextureFromFile(IMAGE_PATH90, &textureID90, &my_image_width, &my_image_height);
    bool img91  = LoadTextureFromFile(IMAGE_PATH91, &textureID91, &my_image_width, &my_image_height);
    bool img92  = LoadTextureFromFile(IMAGE_PATH92, &textureID92, &my_image_width, &my_image_height);
    bool img93  = LoadTextureFromFile(IMAGE_PATH93, &textureID93, &my_image_width, &my_image_height);
    bool img94  = LoadTextureFromFile(IMAGE_PATH94, &textureID94, &my_image_width, &my_image_height);
    bool img95  = LoadTextureFromFile(IMAGE_PATH95, &textureID95, &my_image_width, &my_image_height);
    bool img96  = LoadTextureFromFile(IMAGE_PATH96, &textureID96, &my_image_width, &my_image_height);
    bool img97  = LoadTextureFromFile(IMAGE_PATH97, &textureID97, &my_image_width, &my_image_height);
    bool img98  = LoadTextureFromFile(IMAGE_PATH98, &textureID98, &my_image_width, &my_image_height);
    bool img99  = LoadTextureFromFile(IMAGE_PATH99, &textureID99, &my_image_width, &my_image_height);
 
    IM_ASSERT(img0);   
    IM_ASSERT(img1);
    IM_ASSERT(img2);
    IM_ASSERT(img3);
    IM_ASSERT(img4);
    IM_ASSERT(img5);
    IM_ASSERT(img6);
    IM_ASSERT(img7);
    IM_ASSERT(img8);
    IM_ASSERT(img9);
    IM_ASSERT(img10);   
    IM_ASSERT(img11);
    IM_ASSERT(img12);
    IM_ASSERT(img13);
    IM_ASSERT(img14);
    IM_ASSERT(img15);
    IM_ASSERT(img16);
    IM_ASSERT(img17);
    IM_ASSERT(img18);
    IM_ASSERT(img19);
    IM_ASSERT(img20);   
    IM_ASSERT(img21);
    IM_ASSERT(img22);
    IM_ASSERT(img23);
    IM_ASSERT(img24);
    IM_ASSERT(img25);
    IM_ASSERT(img26);
    IM_ASSERT(img27);
    IM_ASSERT(img28);
    IM_ASSERT(img29);
    IM_ASSERT(img30);   
    IM_ASSERT(img31);
    IM_ASSERT(img32);
    IM_ASSERT(img33);
    IM_ASSERT(img34);
    IM_ASSERT(img35);
    IM_ASSERT(img36);
    IM_ASSERT(img37);
    IM_ASSERT(img38);
    IM_ASSERT(img39);
    IM_ASSERT(img40);   
    IM_ASSERT(img41);
    IM_ASSERT(img42);
    IM_ASSERT(img43);
    IM_ASSERT(img44);
    IM_ASSERT(img45);
    IM_ASSERT(img46);
    IM_ASSERT(img47);
    IM_ASSERT(img48);
    IM_ASSERT(img49);
    IM_ASSERT(img50);   
    IM_ASSERT(img51);
    IM_ASSERT(img52);
    IM_ASSERT(img53);
    IM_ASSERT(img54);
    IM_ASSERT(img55);
    IM_ASSERT(img56);
    IM_ASSERT(img57);
    IM_ASSERT(img58);
    IM_ASSERT(img59);
    IM_ASSERT(img60);   
    IM_ASSERT(img61);
    IM_ASSERT(img62);
    IM_ASSERT(img63);
    IM_ASSERT(img64);
    IM_ASSERT(img65);
    IM_ASSERT(img66);
    IM_ASSERT(img67);
    IM_ASSERT(img68);
    IM_ASSERT(img69);
    IM_ASSERT(img70);   
    IM_ASSERT(img71);
    IM_ASSERT(img72);
    IM_ASSERT(img73);
    IM_ASSERT(img74);
    IM_ASSERT(img75);
    IM_ASSERT(img76);
    IM_ASSERT(img77);
    IM_ASSERT(img78);
    IM_ASSERT(img79);
    IM_ASSERT(img80);   
    IM_ASSERT(img81);
    IM_ASSERT(img82);
    IM_ASSERT(img83);
    IM_ASSERT(img84);
    IM_ASSERT(img85);
    IM_ASSERT(img86);
    IM_ASSERT(img87);
    IM_ASSERT(img88);
    IM_ASSERT(img89);    
    IM_ASSERT(img90);   
    IM_ASSERT(img91);
    IM_ASSERT(img92);
    IM_ASSERT(img93);
    IM_ASSERT(img94);
    IM_ASSERT(img95);
    IM_ASSERT(img96);
    IM_ASSERT(img97);
    IM_ASSERT(img98);
IM_ASSERT(img99);
    

    // Our state
    // bool show_demo_window = true;
    bool show_another_window = false;

    // Main loop
    bool done = false;
    while (!done)
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        
        // if (show_demo_window)
        //     ImGui::ShowDemoWindow(&show_demo_window);

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {

            /*****************************************************************/

            ImGui::Begin("Page de connection pour le BINGO");
            ImGui::Text("Page de connection pour le BINGO"); 

            static char str0[128] = "";
            static char pseudo[128] = "nico";
            static char bufpass[128] = "";
            static char mdp[128] = "nico";

            ImGui::InputText("username", str0, IM_ARRAYSIZE(str0));
            ImGui::InputText("password", bufpass, 64, ImGuiInputTextFlags_Password | ImGuiInputTextFlags_CharsNoBlank);

            if(strlen(str0) == strlen(pseudo) && strlen(bufpass) == strlen(mdp))
            {
                static int clicked = 0;
                if (ImGui::Button("Jouer !"))
                    clicked++;
                if (clicked > 1)
                {

                int txture[100];
                txture[0] = textureID0;
                    txture[1] = textureID1;
                    txture[2] = textureID2;
                    txture[3] = textureID3;
                    txture[4] = textureID4;
                    txture[5] = textureID5;
                    txture[6] = textureID6;
                    txture[7] = textureID7;
                    txture[8] = textureID8;
                    txture[9] = textureID9;
                    txture[10] = textureID10;
                    txture[11] = textureID11;
                    txture[12] = textureID12;
                    txture[13] = textureID13;
                    txture[14] = textureID14;
                    txture[15] = textureID15;
                    txture[16] = textureID16;
                    txture[17] = textureID17;
                    txture[18] = textureID18;
                    txture[19] = textureID19;
                    txture[20] = textureID20;
                    txture[21] = textureID21;
                    txture[22] = textureID22;
                    txture[23] = textureID23;
                    txture[24] = textureID24;
                    txture[25] = textureID25;
                    txture[26] = textureID26;
                    txture[27] = textureID27;
                    txture[28] = textureID28;
                    txture[29] = textureID29;
                    txture[30] = textureID30;
                    txture[31] = textureID31;
                    txture[32] = textureID32;
                    txture[33] = textureID33;
                    txture[34] = textureID34;
                    txture[35] = textureID35;
                    txture[36] = textureID36;
                    txture[37] = textureID37;
                    txture[38] = textureID38;
                    txture[39] = textureID39;
                    txture[40] = textureID40;
                    txture[41] = textureID41;
                    txture[42] = textureID42;
                    txture[43] = textureID43;
                    txture[44] = textureID44;
                    txture[45] = textureID45;
                    txture[46] = textureID46;
                    txture[47] = textureID47;
                    txture[48] = textureID48;
                    txture[49] = textureID49;
                    txture[50] = textureID50;
                    txture[51] = textureID51;
                    txture[52] = textureID52;
                    txture[53] = textureID53;
                    txture[54] = textureID54;
                    txture[55] = textureID55;
                    txture[56] = textureID56;
                    txture[57] = textureID57;
                    txture[58] = textureID58;
                    txture[59] = textureID59;
                    txture[60] = textureID60;
                    txture[61] = textureID61;
                    txture[62] = textureID62;
                    txture[63] = textureID63;
                    txture[64] = textureID64;
                    txture[65] = textureID65;
                    txture[66] = textureID66;
                    txture[67] = textureID67;
                    txture[68] = textureID68;
                    txture[69] = textureID69;
                    txture[70] = textureID70;
                    txture[71] = textureID71;
                    txture[72] = textureID72;
                    txture[73] = textureID73;
                    txture[74] = textureID74;
                    txture[75] = textureID75;
                    txture[76] = textureID76;
                    txture[77] = textureID77;
                    txture[78] = textureID78;
                    txture[79] = textureID79;
                    txture[80] = textureID80;
                    txture[81] = textureID81;
                    txture[82] = textureID82;
                    txture[83] = textureID83;
                    txture[84] = textureID84;
                    txture[85] = textureID85;
                    txture[86] = textureID86;
                    txture[87] = textureID87;
                    txture[88] = textureID88;
                    txture[89] = textureID89;
                    txture[90] = textureID90;
                    txture[91] = textureID91;
                    txture[92] = textureID92;
                    txture[93] = textureID93;
                    txture[94] = textureID94;
                    txture[95] = textureID95;
                    txture[96] = textureID96;
                    txture[97] = textureID97;
                    txture[98] = textureID98;
                txture[99] = textureID99;

                ImGui::Begin("BINGO le Jeu", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
                ImGui::Text("Bienvenu sur BINGO le jeu");


    int i = 0;
    int bc3 = 0;
    int bc2 = 0;
    int bc1 = 0;
    while(i <= 2)// i = 0 ,1, 2 == trois ligne 
    {
        switch(i)
        {
            case 0 : // première ligne
                
                while(bc2 <= 8) // les case de 0 a 8
                {

                tab[i][bc2] = bc2*10 + rand()%9+1; //met un rdm
                bc2 = bc2 + 1;

                }
            break ;

            case 1 ://deuxiemme ligne 
                
                while(bc1 <= 8)
                {

                    tab[i][bc1] = bc1*10 + rand()%9+1; 

                        while(tab[i][bc1] == tab[i-1][bc1])//met rdm dfferent de la 1er ligne 
                        {
                            tab[i][bc1] = bc1*10 + rand()%9+1; 
                        }

                    bc1 = bc1 + 1;

                }
            break;

            case 2 ://troisième ligne
            
                while(bc3 <= 8)
                {

                    tab[i][bc3] = bc3*10 + rand()%9+1; 
                    
                        while((tab[i][bc3] == tab[i-1][bc3]) || (tab[i][bc3] == tab[i-2][bc3]))//met rdm dfferent de la 1er et 2eme ligne 
                        {
                            tab[i][bc3] = bc3*10 + rand()%9+1; 
                        }

                    bc3 = bc3 + 1;

                }
            break;
        }
    i = i + 1;//chnage de ligne
    }
    
    
    //fc create_blank() : Met 4 cases vides par ligne (aléatoire)
    ligne = 0;
    for (ligne = 0 ; ligne < 3; ligne ++)
    
    {
        int case_vide1 = 0;
        int case_vide2 = 0;
        int case_vide3 = 0;
        int case_vide4 = 0;
        case_vide1 = rand()%8+1;
        case_vide2 = rand()%8+1;
            while(case_vide2 == case_vide1)
            {
                case_vide2 = rand()%8+1;
            }

        case_vide3 = rand()%8+1;
            while(case_vide3 == case_vide1 || case_vide3 == case_vide2)
            {
                case_vide3 = rand()%8+1;
            }
        case_vide4 = rand()%8+1;
            while(case_vide4 == case_vide1 || case_vide4 == case_vide2 || case_vide4 == case_vide3)
            {
                case_vide4 = rand()%8+1;
            }
        
        tab[ligne][case_vide1] = 0;
        tab[ligne][case_vide2] = 0;
        tab[ligne][case_vide3] = 0;
        tab[ligne][case_vide4] = 0;
        case_vide1 = 0;
        case_vide2 = 0;
        case_vide3 = 0;
        case_vide4 = 0;

    }
    

/***********************************************************************/

/*
    1- ALGO CREATION NBR ALEATOIRE dans affiche tab 

    2- affiche toutes les vleurs du tableau : 

    3- tab[numero_de_ligne][numero_de_colonne] 

*/

                    int numero_de_ligne = 0;
                    int numero_de_colonne = 0;
                    int temp = rand()%9+1;


                    
                    ImGui::Image((void*)(intptr_t)txture[0], ImVec2(IMG_SIZE, IMG_SIZE));

                    ImGui::Image((void*)(intptr_t)txture[1], ImVec2(IMG_SIZE, IMG_SIZE));

                    ImGui::Image((void*)(intptr_t)txture[temp], ImVec2(IMG_SIZE, IMG_SIZE));



                     if(numero_de_ligne < 3)
                     {      
                         while(numero_de_colonne < 9)
                             {
                                 ImGui::Image((void*)(intptr_t)txture[tab[numero_de_ligne][numero_de_colonne]], ImVec2(IMG_SIZE, IMG_SIZE));
                               
                                 if(numero_de_colonne < 8)
                                 {
                                     ImGui::SameLine();  
                                 }


                                 numero_de_colonne = numero_de_colonne + 1;   
                             }  
                     numero_de_colonne = 0;
                     numero_de_ligne = numero_de_ligne + 1;
                     }
                    

                    if (ImGui::Button("Sortir ?"))
                        show_another_window = false;

                    ImGui::End();
                }

            }
            ImGui::End();

            // Rendering
            ImGui::Render();
            glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
            glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
            glClear(GL_COLOR_BUFFER_BIT);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
            SDL_GL_SwapWindow(window);

            /*****************************************************************/

            // limit to ~ 60 fps is enough
            SDL_Delay(15);
        }
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

