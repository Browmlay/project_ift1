/*
 * Fichier bingo.c pour le projet ift1
 * Auteur : Eric Bachard  / vendredi 27 décembre 2019, 23:09:00 (UTC+0100)
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tirage_bingo.h"


extern int tab[3][9];

void bingo(int tab[][9])
{
    int ligne = 0;
    int colonne = 0;

    time_t t1;
    (void)time(&t1);
    srand((long)t1);

/*************************ALGO DU BINGO FONCTIONNEL***************************/


    // FIXME
    // should move in bingo.cand replaced by something like :
    // bingo();

    int i = 0;
    int bc3 = 0;
    int bc2 = 0;
    int bc1 = 0;

    while(i <= 2)// i = 0 ,1, 2 == trois ligne 
    {
        switch(i)
        {
            case 0 : // première ligne
                
                while(bc2 <= 8) // les case de 0 a 8
                {

                tab[i][bc2] = bc2*10 + rand()%9+1; //met un rdm
                bc2 = bc2 + 1;

                }
            break ;

            case 1 ://deuxiemme ligne 
                
                while(bc1 <= 8)
                {

                    tab[i][bc1] = bc1*10 + rand()%9+1; 

                        while(tab[i][bc1] == tab[i-1][bc1])//met rdm dfferent de la 1er ligne 
                        {
                            tab[i][bc1] = bc1*10 + rand()%9+1; 
                        }

                    bc1 = bc1 + 1;

                }
            break;

            case 2 ://troisième ligne
            
                while(bc3 <= 8)
                {

                    tab[i][bc3] = bc3*10 + rand()%9+1; 
                    
                        while((tab[i][bc3] == tab[i-1][bc3]) || (tab[i][bc3] == tab[i-2][bc3]))//met rdm dfferent de la 1er et 2eme ligne 
                        {
                            tab[i][bc3] = bc3*10 + rand()%9+1; 
                        }

                    bc3 = bc3 + 1;

                }
            break;
        }
    i = i + 1;//chnange de ligne
    }
    
    
    //fc create_blank() : Met 4 cases vides par ligne (aléatoire)
    ligne = 0;
    for (ligne = 0 ; ligne < 3; ligne ++)
    
    {
        int case_vide1 = 0;
        int case_vide2 = 0;
        int case_vide3 = 0;
        int case_vide4 = 0;
        case_vide1 = rand()%8+1;
        case_vide2 = rand()%8+1;
            while(case_vide2 == case_vide1)
            {
                case_vide2 = rand()%8+1;
            }

        case_vide3 = rand()%8+1;
            while(case_vide3 == case_vide1 || case_vide3 == case_vide2)
            {
                case_vide3 = rand()%8+1;
            }
        case_vide4 = rand()%8+1;
            while(case_vide4 == case_vide1 || case_vide4 == case_vide2 || case_vide4 == case_vide3)
            {
                case_vide4 = rand()%8+1;
            }
        
        tab[ligne][case_vide1] = 0;
        tab[ligne][case_vide2] = 0;
        tab[ligne][case_vide3] = 0;
        tab[ligne][case_vide4] = 0;
        case_vide1 = 0;
        case_vide2 = 0;
        case_vide3 = 0;
        case_vide4 = 0;

    }
    
    //fc conversion du tableau 2 dim en 1 dim pour vérif des valeurs.
    colonne = 0;
    ligne = 0;
    int vlr_liste = 0;
    int tab_grid[15]; //15 nbr dans la grille (case vide enlevée)

    while(ligne < 3)
        {
            while(colonne < 9)
            {
                if(tab[ligne][colonne] != 0)
                {
                    tab_grid[vlr_liste] = tab[ligne][colonne];
                    vlr_liste = vlr_liste + 1;
                }
                colonne = colonne + 1;
            }
            colonne = 0;
            ligne = ligne + 1;
        }

#ifdef DEBUG
           for (int l = 0 ; l < 3 ; l++)
           {
               for (int m = 0 ; m < 9 ; m++)
               {
                   fprintf(stdout, "bingo.c : tab[%d][%d] = %d  ;", l, m, tab[l][m]);
               }
               fprintf(stdout, "\n");
           }
           getchar();

#endif
}

/***************************************FIN ALGO BINGO************************************************/
