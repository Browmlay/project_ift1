#include <stdio.h>
#include <stdlib.h>
#include "saisie.h"
#include "efface_ecran.h"
#include "choice.h"
#include "menujeu.h"
#include "affiche_profile.h"
#include "affiche_menu.h"
#include "boucle_jeu.h"


int boucle_jeu(void)
{

    bool b_Quitter = false;

    int anErr = 0;

    do
    {
#ifndef DEBUG
        efface_ecran();
#endif
        affiche_menu();

        switch(saisie_nombre_entier_court(BINGO_NOT_A_CHOICE, BINGO_QUIT))
        {
            case BINGO_DISPLAY_PROFILE:
                anErr = affiche_profile();

                if (anErr != 0)
                    fprintf(stdout,"pb avec affiche_profile()");

                fprintf(stdout,"vous etes sur votre profil");
            break;

            case BINGO_PLAY:
                anErr = boucle_jeu();

                if (anErr != 0)
                    fprintf(stdout,"pb avec boucle_jeu()");

                fprintf(stdout,"vous etes sur votre espace de jeu");
            break;

            case BINGO_QUIT:
                b_Quitter = true;
                fprintf(stdout,"vous quittez la plateforme");
            break;

            case BINGO_NOT_A_CHOICE:
            break;

            default:
            break;
        }
            //case 5 : fprintf(stdout,"vous etes sur le jeu de la bataille navale");
            //break;

    } while (false == b_Quitter);

	//exit
    fprintf(stdout, "Appuyer sur la touche entrée (appelée aussi Return) pour continuer\n");
    getchar();
    return EXIT_SUCCESS;
}

