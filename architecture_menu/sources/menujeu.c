
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "saisie.h"
#include "efface_ecran.h"
#include "choice.h"
#include "menujeu.h"
#include "affiche_menu.h"
#include "boucle_jeu.h"


static int anErr = 0;

int menujeu(void)
{
  bool b_Quitter = false;

    do
    {
#ifndef DEBUG
        efface_ecran();
#endif
    
    fprintf(stdout, "1 - BINGO\n");
    fprintf(stdout, "2 - Bataille naval()\n");
    fprintf(stdout, "3 - Quit to main menu\n");


        switch(saisie_nombre_entier_court(USELESS_CHOICE, QUIT_TO_MAIN_MENU))
        {
            case BINGO_CHOICE:
                anErr = 1;//a la place de 1 le nom de la fonction

                if (anErr != 0)
                    fprintf(stdout,"Le lancement du jeu à ratter");

                fprintf(stdout,"CODE BINGO");

                /****************/

                //CODE DU BINGO 

                /***************/
            break;

            case NAVAL_BATTLE_CHOICE:
                anErr = 1;//a la place de 1 le nom de la fonction

                if (anErr != 0)
                    fprintf(stdout,"Le lancement du jeu à ratter");

                fprintf(stdout,"CODE BATTAILLE NAVALE");
                /****************/

                //CODE BATAILLE NAVAL

                /***************/
            break;

            case QUIT_TO_MAIN_MENU:
                b_Quitter = true;
                fprintf(stdout,"vous quittez la plateforme");
            break;

            case USELESS_CHOICE:
            break;

            default:
            break;
        }


    } while (false == b_Quitter);

    return EXIT_SUCCESS;
}
