/*  fichier main.c  project_ift1 */

#include <stdio.h>
#include <stdlib.h>

#include "choice.h"
#include "efface_ecran.h"
#include "menujeu.h"

int main(void)
{
    efface_ecran();
    menujeu();
    return EXIT_SUCCESS;
}

