#ifndef __AFFICHE_MENU_H
#define __AFFICHE_MENU_H

// affiche le menu, et rien d'autre. Pas besoin de retourner une valeur
void affiche_menu(void);

#endif /* __AFFICHE_MENU_H */
