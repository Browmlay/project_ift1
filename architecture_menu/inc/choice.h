/*
 * fichier d'en tête choice.h permet de définir les cas possibles de manière simple
 * Auteur : mettre le nom du responsable du projet
 * This project is licensed under the GNU General Public License v3.0.
 * See : https://choosealicense.com/licenses/gpl-3.0/
 */

#ifndef __CHOICE_H_
#define __CHOICE_H_

typedef enum Choice
{
    BINGO_NOT_A_CHOICE    = 0,
    BINGO_DISPLAY_PROFILE = 1,
    BINGO_PLAY            = 2,
    BINGO_QUIT            = 3
} CHOICE;

typedef enum ChoiceI
{
	USELESS_CHOICE     = 0,
    BINGO_CHOICE        = 1,
    NAVAL_BATTLE_CHOICE = 2,
    QUIT_TO_MAIN_MENU   = 3
} CHOICEI;
#endif  /* __CHOICE_H_  */
