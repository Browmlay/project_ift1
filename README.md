<h1>Projet d'IFT1</h1>
<p>Ce projet a pour but de mettre en application les connaissances acquises en language C pendant les deux premiers mois de Tronc Commun à l'UTBM, l'équipe a choisi tout d'abord de recréer le jeu du Bingo en language C puis en language C++ en intégrant ImGui pour créer un rendu plus visuel de notre jeu, mais aussi PhpMyAdmin pour créer un espace utilisateur</p>
<br>

<h2>Qui sommes nous ?</h2>
<p>Le chef de projet est Nicolas SAINTVOIRIN, accompagné d' Adrien HUYGHEBAERT, d'Enzo LANZARINI, de Quentin DEMONTROND et de Pierre GASSER. Nous sommes étudiants à l'école d'ingénieur de l'UTBM en tronc commun TC01 (1ère année)</p>
<br>

<h2>Règles et Principe du BINGO</h2>
<p>Le Bingo peut être aussi appelé Loto. On joue au jeu avec une grille comportant 15 numéros entre 0 et 99. Le but du jeu est de cocher tous ces numéros. Pour cela, à chaque tour une boule est tirée, jusqu'à la victoire. Notre jeu va inclure une monnaie virtuelle qui permettra d'acheter des grilles. On gagne de l'argent suivant le nombre de boules qu'il faudrait tirer pour finir la grille</p>
<p>Exemple : Vous cochez tous les numéros de votre grille au bout de 70 numéros alors vous avez gagné 100 crédits, de quoi acheter deux grilles etc. </p>
<br>

<h2>LICENSE</h2>
<p>This project is licensed under the GNU General Public License v3.0.</p>
<p>See : https://choosealicense.com/licenses/gpl-3.0/</p>