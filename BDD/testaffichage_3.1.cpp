//inspiré du code de O. Dartois créé en Juin 2017, https://innovelectronique.fr/2017/06/17/utiliser-le-connecteur-c-pour-la-bdd-mysql/
#include <stdlib.h>
#include <iostream>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <string>

using namespace std;
int main(void)
{
	cout << endl;
	cout << "Connection à la platforme" << endl;

	try {
		sql::Driver* driver;
		sql::Connection* con;
		sql::Statement* stmt;
		sql::ResultSet* res;
                
                sql::Statement* stmtpwd;
                sql::ResultSet* respwd;

		driver = get_driver_instance();
		con = driver -> connect("tcp://127.0.0.1:3306","pierre","tigrette");
		con->setSchema("identification_jeu");
                bool isrespwd;
	        do {
			string pseudo;
			cout << "Pseudo :";
			cin >> pseudo;
			string query = ("SELECT * FROM identifiant WHERE Pseudo = '" +pseudo+"'");
			stmt = con -> createStatement ();
			res = stmt -> executeQuery (query);
         		string password;
                        cout << "Mot de passe :";
                        cin >> password;
                        string querypwd = ("SELECT * FROM (identifiant INNER JOIN pwd ON identifiant.Numero_id = pwd.ID_user) WHERE Password = '" +password+"' AND Pseudo = '"+pseudo+"'");
                        stmtpwd = con -> createStatement ();
                        respwd = stmtpwd -> executeQuery (querypwd);
                        isrespwd = respwd->next();
                        if (!isrespwd){
                        cout << "veulliez réessayer !" << endl;
                        }
                }while (!isrespwd);
                        cout << "mdp valide !";

			//cout << "\t Numero d'ID : ";
			//cout << res -> getString("Numero_id") << endl;
			//cout << "\t Nom : ";
			//cout << res -> getString("Nom") << endl;
			//cout << "\t Prenom : ";
			//cout << res -> getString("Prenom") << endl;
			//cout << "\t Email : ";
			//cout << res -> getString("Email") << endl;
			//cout << res -> getString("Pseudo") << endl;


		delete res;
		delete stmt;
		delete con;

	}
	catch (sql::SQLException &e) {
		// Gestion des execeptions pour déboggage
		cout << "# ERR: " << e.what();
		cout << " (code erreur MySQL: " << e.getErrorCode();
		cout << ", EtatSQL: " << e.getSQLState() << " )" << endl;
	}
	cout << endl;




	return EXIT_SUCCESS;
}
