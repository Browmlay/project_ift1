//inspiré du code de O. Dartois créé en Juin 2017, https://innovelectronique.fr/2017/06/17/utiliser-le-connecteur-c-pour-la-bdd-mysql/
#include <stdlib.h>
#include <iostream>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;
int main(void)
{
	cout << endl;
	cout << "test connection à identification_jeu" << endl;

	try {
		sql::Driver* driver;
		sql::Connection* con;
		sql::Statement* stmt;
		sql::ResultSet* res;

		driver = get_driver_instance();
		con = driver -> connect("tcp://127.0.0.1:3306","pierre","tigrette");
		con->setSchema("identification_jeu"); 

		stmt = con -> createStatement ();
		res = stmt -> executeQuery ("SELECT * FROM identifiant ");
		while (res->next()) {
			cout << "\t Numero d'ID : ";
			cout << res -> getString("Numero_id") << endl;
			cout << "\t Nom : ";
			cout << res -> getString("Nom") << endl;
			cout << "\t Prenom : ";
			cout << res -> getString("Prenom") << endl;
			cout << "\t Email : ";
			cout << res -> getString("Email") << endl;
			cout << "\t Pseudo : ";
			cout << res -> getString("Pseudo") << endl;
		}

		delete res;
		delete stmt;
		delete con;

	}
	catch (sql::SQLException &e) {
		// Gestion des execeptions pour déboggage
		cout << "# ERR: " << e.what();
		cout << " (code erreur MySQL: " << e.getErrorCode();
		cout << ", EtatSQL: " << e.getSQLState() << " )" << endl;
	}
	cout << endl;

	return EXIT_SUCCESS;
}
