#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "efface_ecran.h"
 
int main(int argc, char *argv[])
{

	////éfface l'ecran
	efface_ecran();
	///initialisation de la fonction rand 	
	time_t t1;
	(void)time(&t1);
	srand((long)t1);

	int tab[3][9];
 
 //////////création des nombres aléatoires dans chaque case
	int i = 0;
	while(i <= 2)
	{
		
		tab[i][0] = rand()%9+1 + 1; 
		tab[i][1] = 10 + rand()%9+1;
		tab[i][2] = 20 + rand()%9+1;
		tab[i][3] = 30 + rand()%9+1;
		tab[i][4] = 40 + rand()%9+1;
		tab[i][5] = 50 + rand()%9+1;
		tab[i][6] = 60 + rand()%9+1;
		tab[i][7] = 70 + rand()%9+1;
		tab[i][8] = 80 + rand()%9+1;
		
		i = i + 1;
	}
	
/////////Met 4 case vide par ligne (aléatoire)
	int k = 0; 
	while(k <= 2)
	{
		int n1; 
		int n2;
		int n3; 
		int n4;
		n1 = rand()%8+1;
		n2 = rand()%8+1;
		while(n2 == n1)
		{
			n2 = rand()%8+1;
		}
		n3 = rand()%8+1;
		while(n3 == n1 || n3 == n2)
		{
			n3 = rand()%8+1;
		}
		n4 = rand()%8+1;
		while(n4 == n1 || n4 == n2 || n4 == n3)
		{
			n4 = rand()%8+1;
		}
		tab[k][n1] = 0;
		tab[k][n2] = 0;
		tab[k][n3] = 0;
		tab[k][n4] = 0;	
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;

		k = k + 1 ;
	}
	///////////affiche la grille

	int c = 0 ;
	int l = 0 ;
	
	while(l <= 2)
	{
		while(c <= 8)
			{
				if(tab[l][c] < 10){
					fprintf(stdout,"| 0%d |",tab[l][c]);
					}
				else fprintf(stdout,"| %d |",tab[l][c]);
			c = c + 1;
			}
		c = 0; //reinitialise la variable c à 0
		l = l + 1;
		fprintf(stdout,"\n");
	}

	
//////////////// Tirage des nombres gagnants


	//initialisation de la fonction rand 	
	int val = 1 ;
	int tab_valeur[99];

	//cree un tableau avec des valeurs de 0 à 99
	while(val <= 99)
	{
		tab_valeur[val] = val;
		val = val + 1;
	}

	int cp = 0;
	while(cp < 99)
	{

		//tire nombre aléatoire
		int nbr_tirage = 0;
		nbr_tirage = rand()%99+1;

		//test qu'il n'a pas déjà éte tirée
		while(tab_valeur[nbr_tirage - 1] <= 0)
		{
			nbr_tirage = rand()%99+1;
		}
		//le nombre tiree dans le tableau vas etre remplaver par un zero pour en plus etre choisit
		tab_valeur[nbr_tirage - 1] = 0;
		
		//affichage nbr gagant :
		fprintf(stdout,"Le numéros gagant est le%d !\n", nbr_tirage);
		getchar();
		cp = cp + 1;

	}
	return 0;
}